﻿using System;
using System.Xml.Serialization;
using System.IO;

namespace HClear
{
    [Serializable]
    public class PropertiesApp
    {
        private const string PROPERTIES_FILE = "Properties.xml";

        public int SelectTab { get;  set; }
        public string Directores { get; set; } = " .vs obj bin ";
        public string Files { get;  set; } = "*.xBIM,*.wexBIM,*.jfm";
        public double Top { get; set; }
        public double Left { get; set; }

        public static PropertiesApp Load()
        {
            if (File.Exists(PROPERTIES_FILE))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(PropertiesApp));
                using (FileStream fs = new FileStream(PROPERTIES_FILE, FileMode.OpenOrCreate))
                {
                    PropertiesApp property = (PropertiesApp)formatter.Deserialize(fs);
                    return property;
                }
            }
            return new PropertiesApp();
        }

        internal static void Save(PropertiesApp property)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(PropertiesApp));
            using (FileStream fs = new FileStream(PROPERTIES_FILE, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, property);
            }
        }
    }
}
