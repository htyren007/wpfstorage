﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace HClear
{
    public class ClearDirectory
    {
        private string Path;
        private string Dirs;

        public ClearDirectory(string path, string dirs)
        {
            Path = " " + path + " ";
            Dirs = dirs;
        }
        List<string> deleteDir = new List<string>();

        private void Find(DirectoryInfo dir)
        {
            foreach (DirectoryInfo directory in dir.GetDirectories())
            {
                if (Dirs.Contains(" " + directory.Name + " "))
                    deleteDir.Add(directory.FullName);
                else
                    Find(directory);
            }
        }

        public async Task<int> FindAll()
        {
            DirectoryInfo dir = new DirectoryInfo(Path);
            await Task.Run(() => Find(dir));

            return deleteDir.Count;
        }

        public async Task DeleteAll(Action<int> progress = null, Action<string> errorDir = null)
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < deleteDir.Count; i++)
                {
                    try
                    {
                        string dir = deleteDir[i];

                        Directory.Delete(dir, true);
                        //if (i % 5 == 0) 
                        progress?.Invoke(i);
                    }
                    catch
                    {
                        errorDir?.Invoke(deleteDir[i]);
                    }
                }
            });

            deleteDir.Clear();
        }
    }
}
