﻿using System.Windows;

namespace HClear
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ClearViewModel();
            Closed += MainWindow_Closed;

            PropertiesApp property = PropertiesApp.Load();

            mainTabControls.SelectedIndex = property.SelectTab;
            if (DataContext is ClearViewModel model)
            {
                model.Directores = property.Directores;
                model.Mask = property.Files;
            }
            if (property.Top > 1)
            {
                this.Top = property.Top;
                this.Left = property.Left;
            }
        }

        private void MainWindow_Closed(object sender, System.EventArgs e)
        {
            PropertiesApp property = new PropertiesApp();
            property.SelectTab = mainTabControls.SelectedIndex;
            property.Top = this.Top;
            property.Left = this.Left;
            if (DataContext is ClearViewModel model)
            {
                property.Directores = model.Directores;
                property.Files = model.Mask;
            }

            PropertiesApp.Save(property);

        }
    }
}
