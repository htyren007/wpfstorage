﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace HClear
{
    public class ClearViewModel : ObservableObject
    {
        #region Привязки
        string path;
        public string Path
        {
            get => path;
            set => SetProperty(ref path, value);
        }

        string directory = " .vs obj bin ";
        public string Directores
        {
            get => directory;
            set => SetProperty(ref directory, value);
        }

        string mask = "*.xBIM,*.wexBIM,*.jfm";
        public string Mask
        {
            get => mask;
            set => SetProperty(ref mask, value);
        }

        int max;
        public int Max
        {
            get => max;
            set => SetProperty(ref max, value);
        }
        private bool isWork;
        public bool IsWork
        {
            get => isWork;
            set => SetProperty(ref isWork, value);
        }

        int progress;
        public int Progress
        {
            get => progress;
            set => SetProperty(ref progress, value);
        }
        #endregion

        #region Команды
        public ICommand StartCommand { get; set; }
        public ICommand StartFileCommand { get; }
        public ICommand StopCommand { get; set; }

        private async void ClearDirsMethodAsync()
        {
            IsWork = true;
            List<string> listError = new List<string>();
            ClearDirectory clear = new ClearDirectory(Path, Directores);
            int count = await clear.FindAll();
            if (WinBox.ShowQuestion($"Удалить {count} файлов?", "Подтверждение удаления", "Удалить", "Отмена"))
            {
                Max = count;
                await clear.DeleteAll(
                    (i) => { Progress = i; },
                    (e) => { listError.Add(e); });

                if (listError.Count > 0)
                {
                    StringBuilder message = new StringBuilder("Удаление некоторых файлов потерпело неудачу:\n");
                    listError.ForEach(item => message.AppendLine(item));
                    WinBox.ShowMessage(message.ToString(), "Ошибки удалениия!");
                }
                else
                {
                    WinBox.ShowMessage("Удаление выполнено!", "Завершено!");
                }
            }

            IsWork = false;
        }

        private async void ClearFileMethodAsync()
        {
            IsWork = true;
            List<string> listError = new List<string>();
            ClearFile clear = new ClearFile(Path, Mask);
            int count = await clear.FindAll();
            if (WinBox.ShowQuestion($"Удалить {count} файлов?", "Подтверждение удаления", "Удалить","Отмена"))
            {
                Max = count;
                await clear.DeleteAll(
                    (i)=> { Progress = i; },
                    (e)=> { listError.Add(e); });

                if (listError.Count > 0)
                {
                    StringBuilder message = new StringBuilder("Удаление некоторых файлов потерпело неудачу:\n");
                    listError.ForEach(item => message.AppendLine(item));
                    WinBox.ShowMessage(message.ToString(), "Ошибки удалениия!");
                }
                else
                {
                    WinBox.ShowMessage("Удаление выполнено!", "Завершено!");
                }
            }

            IsWork = false;
        }

        #endregion

        public ClearViewModel()
        {
            Path = AppDomain.CurrentDomain.BaseDirectory;
            StartCommand = new RelayCommand(ClearDirsMethodAsync);
            StartFileCommand = new RelayCommand(ClearFileMethodAsync);
        }
    }
}
