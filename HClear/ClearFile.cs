﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HClear
{
    public class ClearFile
    {
        private string Path;
        private string Mask;

        public ClearFile(string path, string mask)
        {
            Path = " " + path + " ";
            Mask = mask;
        }
        List<FileInfo> deleteFiles = new List<FileInfo>();

        public async Task<int> FindAll()
        {
            DirectoryInfo dir = new DirectoryInfo(Path);
            var regex = GetRegex(Mask);
            await Task.Run(() => Find(dir, regex));

            return deleteFiles.Count;
        }

        private void Find(DirectoryInfo dir, Regex regex)
        {
            foreach (DirectoryInfo directory in dir.GetDirectories())
            {
                Find(directory, regex);
            }

            foreach (FileInfo item in dir.GetFiles())
            {
                if (regex.IsMatch(item.Name))
                {
                    deleteFiles.Add(item);
                }
            }
        }

        /// <summary>
        /// Проверка соответствия имени файла маске
        /// </summary>
        /// <param name="input">Маска файла</param>
        /// <returns>true - файл удовлетворяет маске, иначе false</returns>
        static public Regex GetRegex(string mask)
        {
            string[] exts = mask.Split('|', ',', ';');
            string pattern = string.Empty;
            foreach (string ext in exts)
            {
                pattern += @"^";//признак начала строки
                foreach (char symbol in ext)
                    switch (symbol)
                    {
                        case '.': pattern += @"\."; break;
                        case '?': pattern += @"."; break;
                        case '*': pattern += @".*"; break;
                        default: pattern += symbol; break;
                    }
                pattern += @"$|";//признак окончания строки
            }
            if (pattern.Length == 0) return null;
            pattern = pattern.Remove(pattern.Length - 1);
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex;
        }


        /// <summary>
        /// Удаляет все найденные папки и возвращает количество ошибок
        /// </summary>
        /// <param name="worker"></param>
        /// <returns></returns>
        public async Task DeleteAll(Action<int> progress = null, Action<string> errorFile = null)
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < deleteFiles.Count; i++)
                {
                    try
                    {
                        FileInfo file = deleteFiles[i];

                        file.Delete();
                        //if (i % 5 == 0) 
                        progress?.Invoke(i);
                    }
                    catch (Exception ex)
                    {
                        errorFile?.Invoke(deleteFiles[i].FullName);
                    }
                }
            });

            deleteFiles.Clear();
        }
    }
}
