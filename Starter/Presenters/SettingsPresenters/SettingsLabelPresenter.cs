﻿using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsLabelPresenter : ASettingsPresenter
    {
        public SettingsLabelPresenter()
        {
            Label0 = "Надпись 1";
            Label1 = "Надпись 2";
            Label2 = "Надпись 3";
        }

        internal override void Fill(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i])
                {
                    settings.AddLabel(labels[i]);
                }
            }
        }

        internal override void Update(Settings settings)
        {
        }
    }
}