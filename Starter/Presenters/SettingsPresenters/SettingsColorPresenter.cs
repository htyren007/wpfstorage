﻿using System.Windows.Media;
using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsColorPresenter : ASettingsPresenter
    {
        private Color[] values = new[] { Colors.Black, Colors.Red, Colors.Green };

        public Color Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public Color Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public Color Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i])
                {
                    settings.AddColor($"color{i}", labels[i], values[i]);
                }
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetColor($"color{i}", out Color val))
                {
                    values[i] = val;
                    RaisePropertyChanged($"Value{i}");
                }
            }
        }
    }
}