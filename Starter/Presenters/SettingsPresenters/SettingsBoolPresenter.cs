﻿using WPFStorage.Base;
using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsBoolPresenter : ASettingsPresenter
    {
        private bool[] values = new bool[3];

        public bool Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public bool Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public bool Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i])
                {
                    settings.AddBool($"bool{i}", labels[i], values[i]);
                }
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetBool($"bool{i}", out bool rb))
                {
                    switch (i)
                    {
                        case 0: Value0 = rb; break;
                        case 1: Value1 = rb; break;
                        case 2: Value2 = rb; break;
                    }
                }
            }
        }
    }
}