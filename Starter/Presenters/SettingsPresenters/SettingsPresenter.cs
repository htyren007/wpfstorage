﻿#define TEST_VALUE
using System;
using WPFStorage.Base;
using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsPresenter : ObservableObject
    {
        private string title = "Заголовок окна";
        private string textOk = "Сохранить";
        private string textCancel = "Отменить";

        private string result;
        private string testLabel = "Комбо";
        private string testValue = "1";

        public SettingsBoolPresenter SettingsBool { get; set; }
        public SettingsStringPresenter SettingsString { get; set; }
        public SettingsNumericPresenter SettingsNumeric { get; }
        public SettingsDigitPresenter SettingsDigit { get; }
        public SettingsEnumPresenter SettingsEnum { get; }
        public SettingsLabelPresenter SettingsLabel { get; }
        public SettingsColorPresenter SettingsColor { get; }
        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Result { get => result; set => SetProperty(ref result, value); }
        public string TextOk { get => textOk; set => SetProperty(ref textOk, value); }
        public string TextCancel { get => textCancel; set => SetProperty(ref textCancel, value); }
        public string TestLabel { get => testLabel; set => SetProperty(ref testLabel, value); }
        public string TestValue { get => testValue; set => SetProperty(ref testValue, value); }
        
        public RelayCommand OpenSettingsCommand { get; }

        public SettingsPresenter()
        {
            OpenSettingsCommand = new RelayCommand(OpenSettings);
            SettingsBool = new SettingsBoolPresenter();
            SettingsString = new SettingsStringPresenter();
            SettingsNumeric = new SettingsNumericPresenter();
            SettingsDigit  = new SettingsDigitPresenter();
            SettingsEnum = new SettingsEnumPresenter();
            SettingsLabel = new SettingsLabelPresenter();
            SettingsColor = new SettingsColorPresenter();
        }

        private void OpenSettings()
        {
            Settings settings = new Settings();
            settings.Title = Title;
            settings.TextOk = TextOk;
            settings.TextCancel = TextCancel;

            SettingsBool.Fill(settings);
            SettingsString.Fill(settings);
            SettingsNumeric.Fill(settings);
            SettingsDigit.Fill(settings);
            SettingsEnum.Fill(settings);
            SettingsLabel.Fill(settings);
            SettingsColor.Fill(settings);

#if TEST_VALUE
            settings.Add(new PropertyElement(TestLabel, TestValue)); 
#endif

            var res = settings.OpenDialog();

#if TEST_VALUE
            //if (settings.Get("123") is PropertyElement )
            //{
                
            //}
#endif

            SettingsBool.Update(settings);
            SettingsString.Update(settings);
            SettingsNumeric.Update(settings);
            SettingsDigit.Update(settings);
            SettingsEnum.Update(settings);
            SettingsLabel.Update(settings);
            SettingsColor.Update(settings);

            Result = res.ToString();
        }
    }
}