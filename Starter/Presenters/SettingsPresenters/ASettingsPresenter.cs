﻿using WPFStorage.Base;
using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal abstract class ASettingsPresenter: ObservableObject
    {
        protected bool[] kits = new bool[] { true, false, false, };
        protected string[] labels = new[] { "Параметр логический 1", "Параметр логический 2", "Параметр логический 3", };
        public bool Kit0 { get => kits[0]; set => SetProperty(ref kits[0], value); }
        public bool Kit1 { get => kits[1]; set => SetProperty(ref kits[1], value); }
        public bool Kit2 { get => kits[2]; set => SetProperty(ref kits[2], value); }
        public string Label0 { get => labels[0]; set => SetProperty(ref labels[0], value); }
        public string Label1 { get => labels[1]; set => SetProperty(ref labels[1], value); }
        public string Label2 { get => labels[2]; set => SetProperty(ref labels[2], value); }

        internal abstract void Fill(Settings settings);
        internal abstract void Update(Settings settings);
    }
}