﻿using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsDigitPresenter : ASettingsPresenter
    {
        private double[] values = new[] { 3.15, 3.16, 3.14, };

        public double Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public double Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public double Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            if (kits[0])
            {
                settings.AddDigitBox($"digit0", labels[0])
                    .SetValue(values[0])
                    .SetMinValue(-10)
                    .SetMaxValue(50)
                    .SetInkermenter(0.1)
                    .SetIsInteger(false);
            }
            if (kits[1])
            {
                settings.AddUpDownIterator($"digit1", labels[1])
                    .SetValue(values[1])
                    .SetMinValue(-10)
                    .SetMaxValue(50)
                    .SetInkermenter(0.1)
                    .SetIsInteger(false);
            }
            if (kits[2])
            {
                settings.AddSlider($"digit2", labels[2])
                    .SetValue(values[2])
                    .SetMinimum(-10)
                    .SetMaximum(50)
                    .SetStep(0.1)
                    .SetTickStep(1);
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetInteger($"digit{i}", out int val))
                {
                    values[i] = val;
                    RaisePropertyChanged($"Value{i}");
                }
            }
        }
    }
    internal class SettingsNumericPresenter : ASettingsPresenter
    {
        private int[] values = new[] { 3, 3, 3, };

        public int Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public int Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public int Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            if (kits[0])
            {
                settings.AddDigitBox($"num0", labels[0])
                    .SetValue(values[0])
                    .SetMinValue(0)
                    .SetMaxValue(50)
                    .SetIsInteger(true);
            }
            if (kits[1])
            {
                settings.AddUpDownIterator($"num1", labels[1])
                    .SetValue(values[1])
                    .SetMinValue(10)
                    .SetMaxValue(50)
                    .SetIsInteger(true);
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetInteger($"str{i}", out int val))
                {
                    values[i] = val;
                    RaisePropertyChanged($"Value{i}");
                }
            }
        }
    }
}