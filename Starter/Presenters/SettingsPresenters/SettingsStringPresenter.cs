﻿using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsStringPresenter : ASettingsPresenter
    {
        private string[] values = new string[3] {"", "3", "Ренат"};

        public string Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public string Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public string Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i])
                {
                    settings.AddString($"str{i}", labels[i]).SetValue(values[i]);
                }
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetString($"str{i}", out string rs))
                {
                    values[i] = rs;
                    RaisePropertyChanged($"Value{i}");

                    //switch (i)
                    //{
                    //    case 0: StringValue0 = rs; break;
                    //    case 1: StringValue1 = rs; break;
                    //    case 2: StringValue2 = rs; break;
                    //}
                }
            }
        }
    }
}