﻿using WPFStorage.Settings;

namespace Starter.Presenters
{
    internal class SettingsEnumPresenter : ASettingsPresenter
    {
        public enum TestEnam
        {
            Test1, Test2, Test3, Test4, Test5
        }

        private TestEnam[] values = new[] { TestEnam.Test1, TestEnam.Test1, TestEnam.Test1 };

        public TestEnam Value0 { get => values[0]; set => SetProperty(ref values[0], value); }
        public TestEnam Value1 { get => values[1]; set => SetProperty(ref values[1], value); }
        public TestEnam Value2 { get => values[2]; set => SetProperty(ref values[2], value); }

        internal override void Fill(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i])
                {
                    settings.AddDropDown<TestEnam>($"enam{i}", labels[i], values[i]);
                }
            }
        }

        internal override void Update(Settings settings)
        {
            for (int i = 0; i < kits.Length; i++)
            {
                if (kits[i]
                    && settings.TryGetEnam($"enam{i}", out TestEnam val))
                {
                    values[i] = val;
                    RaisePropertyChanged($"Value{i}");
                }
            }
        }
    }
}