﻿using System;
using System.Windows.Media;
using WPFStorage.Base;
using WPFStorage.Controls.ColorInput;

namespace Starter.Presenters
{
    public class ColorPresenter : ObservableObject
    {
        private Color color = Colors.Red;
        private Color colorSpectr;
        private HsvColor colorHsv;
        private HsvColor spectrHsv;

        public ColorPresenter()
        {
            SetColorCommand = new RelayCommand<string>(SetColor);
        }

        public Color Color
        {
            get => color;
            set
            {
                SetProperty(ref color, value);
                ColorHsv = ColorHelper.ToHsv(value);
            }
        }
        public Color ColorSpectr
        {
            get => colorSpectr;
            set
            {
                SetProperty(ref colorSpectr, value);
                SpectrHsv = ColorHelper.ToHsv(value);
            }
        }

        public HsvColor SpectrHsv
        {
            get => spectrHsv;
            set => SetProperty(ref spectrHsv, value);
        }

        public HsvColor ColorHsv
        {
            get => colorHsv;
            set => SetProperty(ref colorHsv, value);
        }
        public ColorDropDownPresenter ColorDropDown{ get; set; }
        public RelayCommand<string> SetColorCommand { get; }

        private void SetColor(string text)
        {
            ColorSpectr = ColorHelper.ToColor(text);
            Color = ColorSpectr;
        }
    }

    public class ColorDropDownPresenter : ObservableObject
    {
        private Color color;

        public ColorDropDownPresenter()
        {
        }

        public Color Color { get => color; set => SetProperty(ref color, value); }

        
    }
}
