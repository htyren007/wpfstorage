﻿using System;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class JsonShowPresenter : ObservableObject
    {
        private string _json = @"{
    ""_id"":""208"", 
    ""Name"":""Лейсан""
}";
        private bool pressOk;
        private double width = 500;
        private double heigth = 400;
        private string title = "Заголовок окна";
        private string message = "Сообщение";
        private bool textIsActive;
        private string okText = "Ok";
        private string cancelText = "Cancel";
        private bool cancelIsActive = true;

        public JsonShowPresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialog);
        }

        private void OpenDialog()
        {
            var jsonPreview = WinBox.JsonPreview().Json(Json).Height(Heigth).Width(Width).Title(Title)
                .OkText(OkText).CancelText(CancelText).CancelIsActive(CancelIsActive);
            if (TextIsActive)
                jsonPreview.Text(Message);
            PressOk = jsonPreview.OpenDialog();
        }

        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public string OkText { get => okText; set => SetProperty(ref okText, value); }
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        public string Json { get => _json; set => SetProperty(ref _json, value); }
        public bool PressOk { get => pressOk; set => SetProperty(ref pressOk, value); }
        public bool CancelIsActive { get => cancelIsActive; set => SetProperty(ref cancelIsActive, value); }
        public bool TextIsActive { get => textIsActive; set => SetProperty(ref textIsActive, value); }
        public double Width { get => width; set => SetProperty(ref width, value); }
        public double Heigth { get => heigth; set => SetProperty(ref heigth, value); }
        public RelayCommand OpenDialogCommand { get; }
    }
}