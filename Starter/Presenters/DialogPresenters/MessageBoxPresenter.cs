﻿using System;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class MessageBoxPresenter : ObservableObject
    {
        private string title = "Заголовок";
        private string message ="Текст сообщения";
        private int timeout = 100;

        public MessageBoxPresenter()
        {
            OnlyMessageCommand = new RelayCommand(OnlyMessage);
            MessageAndTitleCommand = new RelayCommand(MessageAndTitle);
            OpenDialogCommand = new RelayCommand(OpenDialog);
        }

        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public int Timeout { get => timeout; set => SetProperty(ref timeout, value); }
        public RelayCommand OnlyMessageCommand { get; }
        public RelayCommand OpenDialogCommand { get; }
        public RelayCommand MessageAndTitleCommand { get; }

        private void OnlyMessage()
        {
            WinBox.ShowMessage(Message);
        }

        private void MessageAndTitle()
        {
            WinBox.ShowMessage(Message, Title);
        }

        private void OpenDialog()
        {
            WinBox.ShowMessage(Message, Title, Timeout);
        }
    }
}