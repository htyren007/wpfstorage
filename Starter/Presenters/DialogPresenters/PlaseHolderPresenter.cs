﻿using System.Threading.Tasks;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class PlaseHolderPresenter : ObservableObject
    {
        private string message = "Пожалуйста подождите";
        //private string question;
        private string cancelText = "Отмена";
        private int minimum =  0;
        private int maximum = 200;
        private int speed = 50;

        public PlaseHolderPresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialogAsync);
        }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        //public string Question { get => question; set => SetProperty(ref question, value); }
        public int Minimum { get => minimum; set => SetProperty(ref minimum, value); }
        public int Maximum { get => maximum; set => SetProperty(ref maximum, value); }
        public int Speed { get => speed; set => SetProperty(ref speed, value); }

        public RelayCommand OpenDialogCommand { get; }

        private async void OpenDialogAsync()
        {
            var progress = WinBox.PleaseHold(Message, CancelText);
            progress.Minimum = Minimum;
            progress.Maximum = Maximum;
            progress.CancelText = CancelText;
            //var speed = ParseInt(speedPleaseHolder.Text);
            progress.CancelHandler +=() => WinBox.ShowMessage("Произошел StopHandler");

            for (int i = Minimum; i < Maximum; i++)
            {
                progress.Progress = i;
                await Task.Delay(1000 / Speed);
                if (progress.IsStopped)
                {
                    WinBox.ShowMessage("Произошел IsStopped == true");
                    break;
                }
            }
            progress.Close();
        }
    }
}