﻿using System;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class QuestionBoxPresenter : ObservableObject 
    {
        private string title = "Заголовок";
        private string okText = "Ок";
        private string question = "Текст сообщения";
        //private int timeout = 0;
        private string cancelText = "Отмена";
        private bool okResult;

        public QuestionBoxPresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialog);
        }

        public string OkText { get => okText; set => SetProperty(ref okText, value); }
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Question { get => question; set => SetProperty(ref question, value); }
        public bool OkResult { get => okResult; set => SetProperty(ref okResult, value); }
        public RelayCommand OpenDialogCommand { get; }

        private void OpenDialog()
        {
            var res = WinBox.ShowQuestion(question: Question,
                title: Title,
                okText: OkText,
                cancelText: CancelText);

            OkResult = res;
        }
    }
}