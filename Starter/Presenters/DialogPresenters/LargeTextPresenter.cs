﻿using System;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    public class LargeTextPresenter : APresenter
    {
        public RelayCommand Text1Command { get; }
        public RelayCommand Text2Command { get; }

        public LargeTextPresenter():base()
        {
            Text1Command = new RelayCommand(Text1);
            Text2Command = new RelayCommand(Text2);
        }

        protected override void OpenDialog()
        {
            WinBox.LargeText()
                .SetTitle(Title)
                .SetText(Question)
                .Show();
        }

        private void Text2()
        {
            Question = @"Михаил Лермонтов 
Бородино

— Скажи-ка, дядя, ведь не даром
Москва, спаленная пожаром,
Французу отдана?
Ведь были ж схватки боевые,
Да, говорят, еще какие!
Недаром помнит вся Россия
Про день Бородина!

— Да, были люди в наше время,
Не то, что нынешнее племя:
Богатыри — не вы!
Плохая им досталась доля:
Немногие вернулись с поля…
Не будь на то господня воля,
Не отдали б Москвы!

Мы долго молча отступали,
Досадно было, боя ждали,
Ворчали старики:
«Что ж мы? на зимние квартиры?
Не смеют, что ли, командиры
Чужие изорвать мундиры
О русские штыки?»

И вот нашли большое поле:
Есть разгуляться где на воле!
Построили редут.
У наших ушки на макушке!
Чуть утро осветило пушки
И леса синие верхушки —
Французы тут как тут.

Забил заряд я в пушку туго
И думал: угощу я друга!
Постой-ка, брат мусью!
Что тут хитрить, пожалуй к бою;
Уж мы пойдем ломить стеною,
Уж постоим мы головою
За родину свою!

Два дня мы были в перестрелке.
Что толку в этакой безделке?
Мы ждали третий день.
Повсюду стали слышны речи:
«Пора добраться до картечи!»
И вот на поле грозной сечи
Ночная пала тень.

Прилег вздремнуть я у лафета,
И слышно было до рассвета,
Как ликовал француз.
Но тих был наш бивак открытый:
Кто кивер чистил весь избитый,
Кто штык точил, ворча сердито,
Кусая длинный ус.

И только небо засветилось,
Все шумно вдруг зашевелилось,
Сверкнул за строем строй.
Полковник наш рожден был хватом:
Слуга царю, отец солдатам…
Да, жаль его: сражен булатом,
Он спит в земле сырой.

И молвил он, сверкнув очами:
«Ребята! не Москва ль за нами?
Умремте же под Москвой,
Как наши братья умирали!»
И умереть мы обещали,
И клятву верности сдержали
Мы в Бородинский бой.

Ну ж был денек! Сквозь дым летучий
Французы двинулись, как тучи,
И всё на наш редут.
Уланы с пестрыми значками,
Драгуны с конскими хвостами,
Все промелькнули перед нами,
Все побывали тут.

Вам не видать таких сражений!..
Носились знамена, как тени,
В дыму огонь блестел,
Звучал булат, картечь визжала,
Рука бойцов колоть устала,
И ядрам пролетать мешала
Гора кровавых тел.

Изведал враг в тот день немало,
Что значит русский бой удалый,
Наш рукопашный бой!..
Земля тряслась — как наши груди,
Смешались в кучу кони, люди,
И залпы тысячи орудий
Слились в протяжный вой…

Вот смерклось. Были все готовы
Заутра бой затеять новый
И до конца стоять…
Вот затрещали барабаны —
И отступили бусурманы.
Тогда считать мы стали раны,
Товарищей считать.

Да, были люди в наше время,
Могучее, лихое племя:
Богатыри — не вы.
Плохая им досталась доля:
Немногие вернулись с поля.
Когда б на то не божья воля,
Не отдали б Москвы!";
        }

        private void Text1()
        {
            Question = @"Александр Пушкин
Сказка о рыбаке и рыбке

Жил старик со своею старухой
У самого синего моря;
Они жили в ветхой землянке
Ровно тридцать лет и три года.
Старик ловил неводом рыбу,
Старуха пряла свою пряжу.
Раз он в море закинул невод, —
Пришел невод с одною тиной.
Он в другой раз закинул невод,
Пришел невод с травой морскою.
В третий раз закинул он невод, —
Пришел невод с одною рыбкой,
С непростою рыбкой, — золотою.
Как взмолится золотая рыбка!
Голосом молвит человечьим:
«Отпусти ты, старче, меня в море,
Дорогой за себя дам откуп:
Откуплюсь чем только пожелаешь.»
Удивился старик, испугался:
Он рыбачил тридцать лет и три года
И не слыхивал, чтоб рыба говорила.
Отпустил он рыбку золотую
И сказал ей ласковое слово:
«Бог с тобою, золотая рыбка!
Твоего мне откупа не надо;
Ступай себе в синее море,
Гуляй там себе на просторе».

Воротился старик ко старухе,
Рассказал ей великое чудо.
«Я сегодня поймал было рыбку,
Золотую рыбку, не простую;
По-нашему говорила рыбка,
Домой в море синее просилась,
Дорогою ценою откупалась:
Откупалась чем только пожелаю.
Не посмел я взять с нее выкуп;
Так пустил ее в синее море».
Старика старуха забранила:
«Дурачина ты, простофиля!
Не умел ты взять выкупа с рыбки!
Хоть бы взял ты с нее корыто,
Наше-то совсем раскололось».

Вот пошел он к синему морю;
Видит, — море слегка разыгралось.
Стал он кликать золотую рыбку,
Приплыла к нему рыбка и спросила:
«Чего тебе надобно, старче?»
Ей с поклоном старик отвечает:
«Смилуйся, государыня рыбка,
Разбранила меня моя старуха,
Не дает старику мне покою:
Надобно ей новое корыто;
Наше-то совсем раскололось».
Отвечает золотая рыбка:
«Не печалься, ступай себе с богом,
Будет вам новое корыто».
Воротился старик ко старухе,
У старухи новое корыто.
Еще пуще старуха бранится:
«Дурачина ты, простофиля!
Выпросил, дурачина, корыто!
В корыте много ль корысти?
Воротись, дурачина, ты к рыбке;
Поклонись ей, выпроси уж избу».
Вот пошел он к синему морю,
Будет вам новое корыто».
Воротился старик ко старухе,
Стал он кликать золотую рыбку,
Приплыла к нему рыбка, спросила:
«Чего тебе надобно, старче?»
Ей старик с поклоном отвечает:
«Смилуйся, государыня рыбка!
Еще пуще старуха бранится,
Не дает старику мне покою:
Избу просит сварливая баба».
Отвечает золотая рыбка:
«Не печалься, ступай себе с богом,
Так и быть: изба вам уж будет».
Пошел он ко своей землянке,
А землянки нет уж и следа;
Перед ним изба со светелкой,
С кирпичною, беленою трубою,
С дубовыми, тесовыми вороты.
Старуха сидит под окошком,
На чем свет стоит мужа ругает.
«Дурачина ты, прямой простофиля!
Выпросил, простофиля, избу!
Воротись, поклонися рыбке:
Не хочу быть черной крестьянкой,
Хочу быть столбовою дворянкой».

Пошел старик к синему морю;
(Не спокойно синее море.)
Стал он кликать золотую рыбку.
Приплыла к нему рыбка, спросила:
«Чего тебе надобно, старче?»
Ей с поклоном старик отвечает:
«Смилуйся, государыня рыбка!
Пуще прежнего старуха вздурилась,
Не дает старику мне покою:
Уж не хочет быть она крестьянкой,
Хочет быть столбовою дворянкой».
Отвечает золотая рыбка:
«Не печалься, ступай себе с богом».
Воротился старик ко старухе.
Что ж он видит? Высокий терем.
На крыльце стоит его старуха
В дорогой собольей душегрейке,
Парчовая на маковке кичка,
Жемчуги огрузили шею,
На руках золотые перстни,
На ногах красные сапожки.
Перед нею усердные слуги;
Она бьет их, за чупрун таскает.
Говорит старик своей старухе:
«Здравствуй, барыня сударыня дворянка!
Чай, теперь твоя душенька довольна».
На него прикрикнула старуха,
На конюшне служить его послала.

Вот неделя, другая проходит,
Еще пуще старуха вздурилась:
Опять к рыбке старика посылает.
«Воротись, поклонися рыбке:
Не хочу быть столбовою дворянкой,
А хочу быть вольною царицей».
Испугался старик, взмолился:
«Что ты, баба, белены объелась?
Ни ступить, ни молвить не умеешь,
Насмешишь ты целое царство».
Осердилася пуще старуха,
По щеке ударила мужа.
«Как ты смеешь, мужик, спорить со мною,
Со мною, дворянкой столбовою? —
Ступай к морю, говорят тебе честью,
Не пойдешь, поведут поневоле».

Старичок отправился к морю,
(Почернело синее море.)
Стал он кликать золотую рыбку.
Приплыла к нему рыбка, спросила:
«Чего тебе надобно, старче?»
Ей с поклоном старик отвечает:
«Смилуйся, государыня рыбка!
Опять моя старуха бунтует:
Уж не хочет быть она дворянкой,
Хочет быть вольною царицей».
Отвечает золотая рыбка:
«Не печалься, ступай себе с богом!
Добро! будет старуха царицей!»

Старичок к старухе воротился.
Что ж? пред ним царские палаты.
В палатах видит свою старуху,
За столом сидит она царицей,
Служат ей бояре да дворяне,
Наливают ей заморские вины;
Заедает она пряником печатным;
Вкруг ее стоит грозная стража,
На плечах топорики держат.
Как увидел старик, — испугался!
В ноги он старухе поклонился,
Молвил: «Здравствуй, грозная царица!
Ну, теперь твоя душенька довольна».
На него старуха не взглянула,
Лишь с очей прогнать его велела.
Подбежали бояре и дворяне,
Старика взашеи затолкали.
А в дверях-то стража подбежала,
Топорами чуть не изрубила.
А народ-то над ним насмеялся:
«Поделом тебе, старый невежа!
Впредь тебе, невежа, наука:
Не садися не в свои сани!»

Вот неделя, другая проходит,
Еще пуще старуха вздурилась:
Царедворцев за мужем посылает,
Отыскали старика, привели к ней.
Говорит старику старуха:
«Воротись, поклонися рыбке.
Не хочу быть вольною царицей,
Хочу быть владычицей морскою,
Чтобы жить мне в Окияне-море,
Чтоб служила мне рыбка золотая
И была б у меня на посылках».
Старик не осмелился перечить,
Не дерзнул поперек слова молвить.
Вот идет он к синему морю,
Видит, на море черная буря:
Так и вздулись сердитые волны,
Так и ходят, так воем и воют.
Стал он кликать золотую рыбку.
Приплыла к нему рыбка, спросила:
«Чего тебе надобно, старче?»
Ей старик с поклоном отвечает:
«Смилуйся, государыня рыбка!
Что мне делать с проклятою бабой?
Уж не хочет быть она царицей,
Хочет быть владычицей морскою;
Чтобы жить ей в Окияне-море,
Чтобы ты сама ей служила
И была бы у ней на посылках».
Ничего не сказала рыбка,
Лишь хвостом по воде плеснула
И ушла в глубокое море.
Долго у моря ждал он ответа,
Не дождался, к старухе воротился —
Глядь: опять перед ним землянка;
На пороге сидит его старуха,
А пред нею разбитое корыто.";
        }
    }
}