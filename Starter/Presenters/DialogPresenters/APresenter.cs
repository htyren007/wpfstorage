﻿using WPFStorage.Base;

namespace Starter
{
    public abstract class APresenter: ObservableObject
    {
        private string title = "Заголовок";
        private string okText = "Ок";
        private string question = "Введите цифру";
        private string cancelText = "Отмена";

        protected APresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialog);
        }

        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Question { get => question; set => SetProperty(ref question, value); }
        public string OkText { get => okText; set => SetProperty(ref okText, value); }
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        public RelayCommand OpenDialogCommand { get; }

        protected abstract void OpenDialog();
    }
}