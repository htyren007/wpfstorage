﻿using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class DigitBoxPresenter : APresenter
    {
        private double digit;
        private bool okResult;

        public DigitBoxPresenter() : base()
        {
        }

        public double Value { get => digit; set => SetProperty(ref digit, value); }
        public bool OkResult { get => okResult; set => SetProperty(ref okResult, value); }

        protected override void OpenDialog()
        {
            OkResult = WinBox.DigitBox()
                .Title(Title)
                .Question(Question)
                .OkText(OkText)
                .CancelText(CancelText)
                .SetStartValue(Value)
                .SetStep(0.001)
                .ShowDialog(out double value);

            Value = value;
        }
    }
}