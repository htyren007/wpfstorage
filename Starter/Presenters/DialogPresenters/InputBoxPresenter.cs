﻿using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    internal class InputBoxPresenter : ObservableObject
    {
        private string title = "Заголовок";
        private string okText = "Ок";
        private string question = "Введите строку";
        private string cancelText = "Отмена";
        private string input = string.Empty;
        private bool result;
        private bool okIsActive = true;
        private bool cancelIsActive = true;

        public InputBoxPresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialog);
            OpenShowInputCommand = new RelayCommand(OpenShowInput);
        }

        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Question { get => question; set => SetProperty(ref question, value); }
        public string Input { get => input; set => SetProperty(ref input, value); }
        public bool OkResult { get => result; set => SetProperty(ref result, value); }
        public bool OkIsActive { get => okIsActive; set => SetProperty(ref okIsActive, value); }
        public bool CanсelIsActive { get => cancelIsActive; set => SetProperty(ref cancelIsActive, value); }
        public string OkText { get => okText; set => SetProperty(ref okText, value); }
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        public RelayCommand OpenDialogCommand { get; }
        public RelayCommand OpenShowInputCommand { get; }

        private void OpenDialog()
        {
            var box = WinBox.Input()
                .Value(Input)
                .Title(Title)
                .OkText(OkText)
                .CancelText(CancelText)
                .Question(Question)
                .OkIsActive(OkIsActive)
                .CancelIsActive(CanсelIsActive);

            var res = box.ShowDialog(out string result);
            
            if (OkResult = res)
                Input = result;
        }

        private void OpenShowInput()
        {
            var res = WinBox.ShowInput(Question, Title, Input);
            if (OkResult = res != null)
            {
                Input = res;
            }
        }
    }
}