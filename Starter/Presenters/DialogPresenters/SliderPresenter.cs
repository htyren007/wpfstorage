﻿using WPFStorage.Dialogs;

namespace Starter
{
    internal class SliderPresenter : APresenter
    {
        private bool result;
        private double value;
        private bool isActiveOk = true;
        private bool isActiveCancel;
        private double minimum = 0;
        private double maximum = 10;
        private double step = 0.01;

        public SliderPresenter() : base()
        {
            Question = "Введите число";
        }

        public double Value { get => value; set => SetProperty(ref this.value, value); }
        public double Minimum { get => minimum; set => SetProperty(ref minimum, value); }
        public double Maximum { get => maximum; set => SetProperty(ref maximum, value); }
        public double Step { get => step; set => SetProperty(ref step, value); }
        public bool Result { get => result; set => SetProperty(ref result, value); }
        public bool IsActiveOk { get => isActiveOk; set => SetProperty(ref isActiveOk, value); }
        public bool IsActiveCancel { get => isActiveCancel; set => SetProperty(ref isActiveCancel, value); }

        protected override void OpenDialog()
        {
            var res = WinBox.Slider()
                .SetTitle(Title)
                .SetOkText(OkText)
                .SetActiveOk(IsActiveOk)
                .SetActiveCancel(IsActiveCancel)
                .SetMinimum(Minimum)
                .SetMaximum(Maximum)
                .SetStep(Step)
                .SetCancelText(CancelText)
                .SetQuestion(Question)
                .Show(out double value);

            Result = res;
            Value = value;
            
        }
    }
}