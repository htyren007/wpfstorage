﻿using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace Starter
{
    //internal class NumericBoxPresenter : ObservableObject
    //{
    //    private string title = "Заголовок";
    //    private string result;
    //    private string question = "Текст сообщения";
    //    private string collect = "Один Два Три Четыре Пять";

    //    public NumericBoxPresenter()
    //    {
    //        OpenDialogCommand = new RelayCommand(OpenDialog);
    //    }

    //    public string Title { get => title; set => SetProperty(ref title, value); }
    //    public string Question { get => question; set => SetProperty(ref question, value); }
    //    public string Collect { get => collect; set => SetProperty(ref collect, value); }
    //    public string Result { get => result; set => SetProperty(ref result, value); }
    //    public RelayCommand OpenDialogCommand { get; }

    //    private void OpenDialog()
    //    {
    //        var items = Collect.Split(' ');

    //        var res = WinBox.SelectorBox(
    //            collect: items,
    //            question: Question,
    //            title: Title);

    //        Result = res;
    //    }
    //}

    internal class SelectorBoxPresenter : ObservableObject
    {
        private string title = "Заголовок";
        private string result = "Ок";
        private string question = "Текст сообщения";
        private string collect = "Один Два Три Четыре Пять";

        public SelectorBoxPresenter()
        {
            OpenDialogCommand = new RelayCommand(OpenDialog);
        }

        public string Title { get => title; set => SetProperty(ref title, value); }
        public string Question { get => question; set => SetProperty(ref question, value); }
        public string Collect { get => collect; set => SetProperty(ref collect, value); }
        public string Result { get => result; set => SetProperty(ref result, value); }
        public RelayCommand OpenDialogCommand { get; }

        private void OpenDialog()
        {
            var items = Collect.Split(' ');

            var box = WinBox.Selector().Question(question).Title(title);

            foreach (var item in items)
            {
                box.AddItem(item);
            }

            box.Show();

            Result = box.SelectItem;

            //var res = WinBox.SelectoBox(
            //    collect: items,
            //    question: Question,
            //    title: Title);

            //Result = res;
        }
    }
}