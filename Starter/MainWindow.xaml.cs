﻿using System;
using System.Windows;

namespace Starter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
            Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            MainConfig.Tabs.SetNumeric("main", mainTab.SelectedIndex);
            MainConfig.Save();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //MainConfig.Load();

            var index = (int)MainConfig.Tabs.GetNumeric("main");
            this.mainTab.SelectedIndex = index;
        }
    }
}
