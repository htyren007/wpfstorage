﻿using Starter.Presenters;
using System.Windows.Controls;
using WPFStorage.Controls.ColorInput;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для ColorSelectorShow.xaml
    /// </summary>
    public partial class ColorSelectorShow : UserControl
    {
        

        public ColorSelectorShow()
        {
            InitializeComponent();
            DataContext = new ColorPresenter();

            Loaded += ColorSelectorShow_Loaded;
            MainConfig.BeforeSaveConfig += MainConfig_BeforeSaveConfig;
        }

        private void MainConfig_BeforeSaveConfig()
        {
            if (DataContext is ColorPresenter model)
            {
                MainConfig.Colors.SetString("rect", model.Color.ToString());
                MainConfig.Colors.SetString("spectr", model.ColorSpectr.ToString());
            }
        }

        private void ColorSelectorShow_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
        }

        private void ColorSelectorShow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (DataContext is ColorPresenter model)
            {
                var str = MainConfig.Colors.GetString("rect", "red");
                model.Color = ColorHelper.ToColor(str);
                str = MainConfig.Colors.GetString("spectr", "red");
                model.ColorSpectr = ColorHelper.ToColor(str);
            }
        }
    }
}
