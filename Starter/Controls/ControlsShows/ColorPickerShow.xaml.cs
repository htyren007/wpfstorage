﻿using System.Windows;
using System.Windows.Controls;
using WPFStorage.Controls.ColorInput;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для ColorPickerShow.xaml
    /// </summary>
    public partial class ColorPickerShow : UserControl
    {
        private const string PICKER_KEY = "Picker";

        public ColorPickerShow()
        {
            InitializeComponent();
            Loaded += ColorPickerShow_Loaded;
            MainConfig.BeforeSaveConfig += MainConfig_BeforeSaveConfig;
        }

        private void MainConfig_BeforeSaveConfig()
        {
            MainConfig.Colors.SetString(PICKER_KEY, colorPicker.Color.ToString());
        }

        private void ColorPickerShow_Loaded(object sender, RoutedEventArgs e)
        {
            var str = MainConfig.Colors.GetString(PICKER_KEY, "red");
            this.colorPicker.Color = ColorHelper.ToColor(str);
        }
    }
}
