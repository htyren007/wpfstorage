﻿using System.Windows.Controls;
using WPFStorage.Controls.ColorInput;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для ColorDropDownShow.xaml
    /// </summary>
    public partial class ColorDropDownShow : UserControl
    {
        private const string DROPDOWN_KEY = "DropDown";

        public ColorDropDownShow()
        {
            InitializeComponent();
            Loaded += ColorDropDownShow_Loaded;
            MainConfig.BeforeSaveConfig += MainConfig_BeforeSaveConfig; 
        }

        private void MainConfig_BeforeSaveConfig()
        {
            MainConfig.Colors.SetString(DROPDOWN_KEY, colorDropDown.Selected.ToString());
        }

        private void ColorDropDownShow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var str = MainConfig.Colors.GetString(DROPDOWN_KEY, "red");
            this.colorDropDown.Selected = ColorHelper.ToColor(str);
        }
    }
}
