﻿using Starter.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для ControlsTest.xaml
    /// </summary>
    public partial class ControlsTest : UserControl
    {
        public ControlsTest()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            Unloaded += ControlsTest_Unloaded;
            DataContext = new ColorPresenter();
        }

        private void ControlsTest_Unloaded(object sender, RoutedEventArgs e)
        {
            dialog.SelectionChanged -= Dialog_SelectionChanged;
            Loaded -= OnLoaded;
            Unloaded -= ControlsTest_Unloaded;

        }

        private void OnLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            dialog.SelectedIndex = (int)MainConfig.Tabs.GetNumeric("controls");
            dialog.SelectionChanged += Dialog_SelectionChanged;
        }

        private void Dialog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainConfig.Tabs.SetNumeric("controls", dialog.SelectedIndex);
        }
    }
}
