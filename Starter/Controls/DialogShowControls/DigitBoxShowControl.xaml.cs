﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для DigitBoxShowControl.xaml
    /// </summary>
    public partial class DigitBoxShowControl : UserControl
    {
        public DigitBoxShowControl()
        {
            InitializeComponent();
            DataContext = new DigitBoxPresenter();
        }
    }
}
