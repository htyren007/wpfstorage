﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для DialogsUserControl.xaml
    /// </summary>
    public partial class DialogsUserControl : UserControl
    {
        public DialogsUserControl()
        {
            InitializeComponent();
            DataContext = new DialogPresenter();
            Loaded += DialogsUserControl_Loaded;
            Unloaded += DialogsUserControl_Unloaded;
        }

        private void DialogsUserControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            dialog.SelectionChanged -= Dialog_SelectionChanged;
            Loaded -= DialogsUserControl_Loaded;
            Unloaded -= DialogsUserControl_Unloaded;
        }

        private void DialogsUserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            dialog.SelectedIndex = (int)MainConfig.Tabs.GetNumeric("dialog");
            dialog.SelectionChanged += Dialog_SelectionChanged;
        }

        private void Dialog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainConfig.Tabs.SetNumeric("dialog", dialog.SelectedIndex);
        }
    }
}
