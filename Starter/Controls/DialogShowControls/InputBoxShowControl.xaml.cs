﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для InputBoxShowControl.xaml
    /// </summary>
    public partial class InputBoxShowControl : UserControl
    {
        public InputBoxShowControl()
        {
            InitializeComponent();
            DataContext = new InputBoxPresenter();
        }
    }
}
