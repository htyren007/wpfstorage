﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для BigTextShow.xaml
    /// </summary>
    public partial class BigTextShow : UserControl
    {
        public BigTextShow()
        {
            InitializeComponent();
            DataContext = new LargeTextPresenter();
        }
    }
}
