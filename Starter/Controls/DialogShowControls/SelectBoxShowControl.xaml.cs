﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для SelectBoxShowControl.xaml
    /// </summary>
    public partial class SelectBoxShowControl : UserControl
    {
        public SelectBoxShowControl()
        {
            InitializeComponent();
            DataContext = new SelectorBoxPresenter();
        }
    }
}
