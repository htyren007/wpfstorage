﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для MessageBoxShowControl.xaml
    /// </summary>
    public partial class MessageBoxShowControl : UserControl
    {
        public MessageBoxShowControl()
        {
            InitializeComponent();
            DataContext = new MessageBoxPresenter();
        }
    }
}
