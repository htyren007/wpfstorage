﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для JsonShowControl.xaml
    /// </summary>
    public partial class JsonShowControl : UserControl
    {
        public JsonShowControl()
        {
            InitializeComponent();
            DataContext = new JsonShowPresenter();
        }
    }
}
