﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для PlaseHolderControl.xaml
    /// </summary>
    public partial class PlaseHolderControl : UserControl
    {
        public PlaseHolderControl()
        {
            InitializeComponent();
            DataContext = new PlaseHolderPresenter();
        }
    }
}
