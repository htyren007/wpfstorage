﻿using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для SliderBoxShowControl.xaml
    /// </summary>
    public partial class SliderBoxShowControl : UserControl
    {
        public SliderBoxShowControl()
        {
            InitializeComponent();
            DataContext = new SliderPresenter();
        }
    }
}
