﻿using Starter.Presenters;
using System.Windows.Controls;

namespace Starter.Controls
{
    /// <summary>
    /// Логика взаимодействия для SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public SettingsControl()
        {
            InitializeComponent();
            DataContext = new SettingsPresenter();
            Unloaded += SettingsControl_Unloaded;
            Loaded += SettingsControl_Loaded;
            
        }

        private void SettingsControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Unloaded -= SettingsControl_Unloaded;
            Loaded -= SettingsControl_Loaded;
            settings.SelectionChanged -= Settings_SelectionChanged;
        }

        private void SettingsControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            settings.SelectedIndex = (int)MainConfig.Tabs.GetNumeric("settings");
            settings.SelectionChanged += Settings_SelectionChanged;
        }

        private void Settings_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainConfig.Tabs.SetNumeric("settings", settings.SelectedIndex);
        }
    }
}
