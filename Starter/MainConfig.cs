﻿using hty.Data;
using System;

namespace Starter
{
    public static class MainConfig
    {
        private const string COLORS_KEY = "colors";
        private const string TABS_KEY = "TABS";
        public static event Action BeforeSaveConfig; 
        public static HIniData Config { get; private set; }
        public static Group Tabs => Config.GetGroup(TABS_KEY);
        public static Group Colors => Config.GetGroup(COLORS_KEY);

        static MainConfig()
        {
            Load();
        }

        public static void Load()
        {
            Config = HIniData.LoadFile("Configure.ini");
        }

        internal static void Save()
        {
            BeforeSaveConfig?.Invoke();
            Config.SaveFile();
        }
    }
}
