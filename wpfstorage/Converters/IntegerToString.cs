﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFStorage.Converters
{
    public class IntegerToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int ivalue)
            {
                return ivalue.ToString();
            }
            return value.ToString();

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string svalue)
            {
                if (int.TryParse(svalue, out int ivalue))
                    return ivalue;
            }
            return 0;
        }
    }
}
