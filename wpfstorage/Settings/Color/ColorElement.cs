﻿using System.Windows.Controls;
using System.Windows.Media;

namespace WPFStorage.Settings
{
    public class ColorElement : ElementBase
    {
        private Color value;
        private string toolTip;

        public ColorElement(string name, string displayName, Color? value = null) : base(name, displayName)
        {
            this.Value = value ?? Colors.Red;
        }

        public Color Value { get => value; set { SetProperty(ref this.value, value); IsDirty = true; } }
    
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }

        public override object GetValue() => Value;


        public override UserControl GetView()
        {
            var control = new ColorControl() 
            { 
                DataContext = this 
            };
            return control;
        }
    }
}