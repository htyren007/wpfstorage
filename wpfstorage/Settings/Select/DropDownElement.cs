﻿using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace WPFStorage.Settings
{
    public class DropDownElement : ElementBase
    {
        private string selectedItem;

        public ObservableCollection<string> Collection { get; set; } = new ObservableCollection<string>();

        public string SelectedItem
        {
            get => selectedItem;
            set
            {
                SetProperty(ref selectedItem, value);
                IsDirty = true;
            }
        }

        public DropDownElement(string key, string displayName) : base(key, displayName)
        {
        }

        public override UserControl GetView()
        {
            return new DropDownControl(this);
        }

        internal void SetCollection(IEnumerable<string> values)
        {
            Collection.Clear();
            foreach (var item in values)
            {
                Collection.Add(item);
            }
        }

        public override object GetValue() => SelectedItem;
    }
}