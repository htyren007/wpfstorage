﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для BoolControl.xaml
    /// </summary>
    public partial class DropDownControl : UserControl
    {
        internal DropDownControl(DropDownElement element)
        {
            InitializeComponent();
            DataContext = element;
        }
    }
}
