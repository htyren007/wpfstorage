﻿using System.Windows;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow(Settings viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
