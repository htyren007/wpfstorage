﻿using System;
using System.Collections.Generic;
using WPFStorage.Base;
using System.Linq;
using System.Windows.Media;
using WPFStorage.Settings.Calendar;

namespace WPFStorage.Settings
{
    public class Settings : ObservableObject
    {
        #region Fields and constructor
        private string title = "Настройки";
        private string textOk = "Сохранить";
        private string textCancel = "Отмена";

        private bool result;
        private SettingsWindow window;

        public Settings()
        {
            OkCommand = new RelayCommand(OkMethod);
            CancelCommand = new RelayCommand(CancelMethod);
            Elements = new List<ElementBase>();
        }
        #endregion

        #region properties
        public string Title { get => title; set => SetProperty(ref title, value); }

        public string TextOk { get => textOk; set => SetProperty(ref textOk, value); }

        public string TextCancel { get => textCancel; set => SetProperty(ref textCancel, value); }
        public bool Result { get => result; internal set => SetProperty(ref result, value); }
        public RelayCommand OkCommand { get; }
        public RelayCommand CancelCommand { get; }
        public List<ElementBase> Elements { get; private set; } 
        #endregion

        #region Configure method
        public void Add(ElementBase element)
        {
            Elements.Add(element);
        }

        public void AddLabel(string label, string toolTip = null)
        {
            Elements.Add(new LabelElement(label) { ToolTip = toolTip });
        }

        public void AddBool(string key, string displayName, bool value, string toolTip = null)
        {
            Elements.Add(new BoolElement(key, displayName, value) { ToolTip = toolTip });
        }

        public StringConfigure  AddString(string key, string displayName)
        {
            var element = new StringElement(key, displayName, "");
            Elements.Add(element);
            return new StringConfigure(element);
        }

        public DigitConfigure AddUpDownIterator(string key, string displayName)
        {
            var element = new UpDownDigitElement(key, displayName);
            Elements.Add(element);
            return new DigitConfigure(element);
        }

        public DigitConfigure AddDigitBox(string key, string displayName)
        {
            var element = new DigitElement(key, displayName);
            Elements.Add(element);
            return new DigitConfigure(element);
        }

        public CalendarConfigure AddCalendarBox(string key, string displayName)
        {
            var element = new CalendarElement(key, displayName);
            Elements.Add(element);
            return new CalendarConfigure(element);
        }

        public SliderConfigure AddSlider(string key, string displayName)
        {
            var element = new SliderElement(key, displayName);
            Elements.Add(element);
            return new SliderConfigure(element);
        }

        public void AddDropDown<TEnam>(string key, string displayName, TEnam value) where TEnam : System.Enum
        {
            if (!typeof(TEnam).IsEnum)
            {
                throw new ArgumentException("TEnam must be an enumerated type");
            }

            var values = Enum.GetNames(typeof(TEnam));
            var element = new DropDownElement(key, displayName);
            Elements.Add(element);

            foreach (var item in values)
            {
                element.Collection.Add(item.ToString());
            }

            element.SelectedItem = value.ToString();
        } 
        #endregion

        #region Get methods
        public bool TryGetEnam<TEnam>(string key, out TEnam value) where TEnam : struct, Enum
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is string str)
            {
                if (Enum.TryParse(str, out TEnam res))
                {
                    value = res;
                    return true;
                }
            }

            value = default;
            return false;
        }

        public bool TryGetBool(string key, out bool value)
        {
            var element = GetElement(key) as BoolElement;
            if (element != null)
            {
                value = element.Value;
                return true;
            }

            value = default;
            return false;
        }
        public bool TryGetString(string key, out string value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is string str)
            {
                value = str;
                return true;
            }

            value = default;
            return false;
        }
        public bool TryGetInteger(string key, out int value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is double v_digit)
            {
                value = (int)Math.Round(v_digit);
                return true;
            }

            value = default;
            return false;
        }
        public bool TryGetFloat(string key, out float value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is double v_digit)
            {
                value = (float)v_digit;
                return true;
            }

            value = default;
            return false;
        }

        public bool TryGetDouble(string key, out double value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is double v_digit)
            {
                value = v_digit;
                return true;
            }

            value = default;
            return false;
        }

        public bool TryGetDateTime(string key, out DateTime value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is DateTime v_datetime)
            {
                value = v_datetime;
                return true;
            }

            value = default;
            return false;
        }

        public ElementBase GetElement(string key)
        {
            return Elements.Find(x => x.Key == key);
        }

        public object GetValue(string key)
        {
            return GetElement(key)?.GetValue();
        }
        #endregion
        #region Color
        public void AddColor(string key, string displayName, Color color)
        {
            var element = new ColorElement(key, displayName, color);
            Elements.Add(element);
        }
        public bool TryGetColor(string key, out Color value)
        {
            var element = GetElement(key);
            if (element != null && element.GetValue() is Color v_color)
            {
                value = v_color;
                return true;
            }

            value = default;
            return false;
        } 
        #endregion


        public bool OpenDialog()
        {
            window = new SettingsWindow(this);
            CreateElement();
            window.ShowDialog();

            return Result;
        }

        public void Open(Action<Settings> closeSetting)
        {
            window = new SettingsWindow(this);
            CreateElement();
            window.Closed += (a,e) => closeSetting(this);
            window.Show();
        }

        private void CreateElement()
        {
            foreach (var element in Elements)
            {
                window.contanier.Children.Add(element.GetView());
            }
        }

        private void CancelMethod()
        {
            Result = false;
            window.Close();
        }

        private void OkMethod()
        {
            Result = true;
            window.Close();
        }
    }
}
