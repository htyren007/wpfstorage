﻿using System;

namespace WPFStorage.Settings
{
    public class DigitConfigure
    {
        private DigitElement element;

        internal DigitConfigure(DigitElement element)
        {
            this.element = element;
        }

        public DigitConfigure SetValue(double value)
        {
            element.Value = value;
            return this;
        }

        public DigitConfigure SetMaxValue(double maxValue)
        {
            element.MaxValue = maxValue;
            return this;
        }

        public DigitConfigure SetIsInteger(bool isInteger)
        {
            element.IsInteger = isInteger;
            return this;
        }

        public DigitConfigure SetMinValue(double minValue)
        {
            element.MinValue = minValue;
            return this;
        }

        public DigitConfigure SetToolTip(string toolTip)
        {
            element.ToolTip = toolTip;
            return this;
        }

        public DigitConfigure SetInkermenter(double ink)
        {
            element.Inkermenter = ink;
            return this;
        }
    }
}