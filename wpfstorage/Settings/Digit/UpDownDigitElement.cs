﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    internal class UpDownDigitElement : DigitElement
    {
        public UpDownDigitElement(string key, string displayName) : base(key, displayName)
        {
        }

        public override UserControl GetView()
        {
            return new UpDownControl(this);
        }
    }
}