﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для SliderElement.xaml
    /// </summary>
    public partial class SliderControl : UserControl
    {
        public SliderControl(SliderElement model)
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
