﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    public class SliderElement : ElementBase
    {
        private string toolTip;
        private double tickStep = 1;
        private double maximum = 100;
        private double minimum = 0;
        private double digit = 0;
        private double step = 1;

        public SliderElement(string key, string displayName) : base(key, displayName)
        {
        }

        public double Value
        {
            get => digit;
            set
            {
                double delta = value % Step;
                if (delta < Step / 2)
                    SetProperty(ref digit, value - delta);
                else
                    SetProperty(ref digit, value - delta + Step);
                IsDirty = true;
            }
        }
        public double Minimum { get => minimum; set => SetProperty(ref minimum, value); }
        public double Maximum { get => maximum; set => SetProperty(ref maximum, value); }
        public double TickStep { get => tickStep; set => SetProperty(ref tickStep, value); }
        public double Step { get => step; set => SetProperty(ref step, value); }
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }

        public override object GetValue() => Value;

        public override UserControl GetView()
        {
            return new SliderControl(this);
        }
    }
    
}