﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для NumericIteratorControl.xaml
    /// </summary>
    public partial class UpDownControl : UserControl
    {
        internal UpDownControl(UpDownDigitElement model)
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
