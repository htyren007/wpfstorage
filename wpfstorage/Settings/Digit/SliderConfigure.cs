﻿using System;

namespace WPFStorage.Settings
{
    public class SliderConfigure
    {
        private SliderElement element;

        public SliderConfigure(SliderElement element)
        {
            this.element = element;
        }

        public SliderConfigure SetValue(double value)
        {
            element.Value = value;
            return this;
        }

        public SliderConfigure SetToolTip(string toolTip)
        {
            element.ToolTip = toolTip;
            return this;
        }

        public SliderConfigure SetMaximum(double maximum)
        {
            element.Maximum = maximum;
            return this;
        }


        public SliderConfigure SetMinimum(double minimum)
        {
            element.Minimum = minimum;
            return this;
        }

        public SliderConfigure SetStep(double step)
        {
            element.Step = step;
            return this;
        }


        public SliderConfigure SetTickStep(double step)
        {
            element.TickStep = step;
            return this;
        }
    }
}