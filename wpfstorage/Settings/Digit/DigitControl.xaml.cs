﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для DigitControl.xaml
    /// </summary>
    public partial class DigitControl : UserControl
    {
        internal DigitControl(DigitElement model)
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
