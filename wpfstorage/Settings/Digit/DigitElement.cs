﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    internal class DigitElement : ElementBase
    {
        private double value = 0;
        private string toolTip;
        private bool isInteger = false;
        private double maxValue = int.MaxValue;
        private double minValue = int.MinValue;
        private double inkermenter = 1;

        public DigitElement(string key, string displayName) : base(key, displayName)
        {
        }

        public double Value
        {
            get => value;
            set
            {
                if (value > MaxValue)
                    SetProperty(ref this.value, MaxValue);
                else if (value < MinValue)
                    SetProperty(ref this.value, MinValue);
                else
                    SetProperty(ref this.value, value);
                IsDirty = true;
            }
        }

        public bool IsInteger { get => isInteger; set => SetProperty(ref isInteger, value); }
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }
        public double MaxValue { get => maxValue; set => SetProperty(ref maxValue, value); }
        public double MinValue { get => minValue; set => SetProperty(ref minValue, value); }
        public double Inkermenter { get => inkermenter; set => SetProperty(ref inkermenter, value); }

        public override UserControl GetView()
        {
            return new DigitControl(this);
        }

        public override object GetValue() => Value;

    }
}