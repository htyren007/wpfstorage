﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPFStorage.Settings.Calendar
{
    public class CalendarElement : ElementBase
    {
        private DateTime value;

        public CalendarElement(string key, string displayName) : base(key, displayName)
        {
        }

        public DateTime Value
        {
            get => value;
            set
            {
                SetProperty(ref this.value, value);
                IsDirty = true;
            }
        }

        public override object GetValue() => Value;

        public override UserControl GetView()
        {
            return new DatePickerControl(this);
        }
    }

    public class CalendarConfigure
    {
        private CalendarElement element;

        public CalendarConfigure SetValue(DateTime value)
        {
            element.Value = value;
            return this;
        }

        public CalendarConfigure(CalendarElement element)
        {
            this.element = element;
        }
    }
}
