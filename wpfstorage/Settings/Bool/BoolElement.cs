﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{

    public class BoolElement : ElementBase
    {
        private bool value;
        private string toolTip;

        public BoolElement(string name, string displayName, bool value) : base(name, displayName)
        {
            this.Value = value;
        }

        public bool Value { get => value; set { SetProperty(ref this.value, value); IsDirty = true; } }
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }

        public override object GetValue() => Value;

        public override UserControl GetView()
        {
            return new BoolControl(this);
        }
    }
}