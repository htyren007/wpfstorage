﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для BoolControl.xaml
    /// </summary>
    public partial class BoolControl : UserControl
    {
        public BoolControl(BoolElement element)
        {
            InitializeComponent();
            DataContext = element;
        }
    }
}
