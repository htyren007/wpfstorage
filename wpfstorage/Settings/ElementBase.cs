﻿using System.Windows.Controls;
using WPFStorage.Base;

namespace WPFStorage.Settings
{
    public abstract class ElementBase : ObservableObject
    {
        protected string key;
        protected string displayName;

        protected ElementBase(string key, string displayName)
        {
            this.key = key;
            this.displayName = displayName;
        }

        public string Key => key;
        public string DisplayName => displayName;
        public bool IsDirty { get; protected set; }

        public abstract UserControl GetView();
        public abstract object GetValue();
    }
}