﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    public class StringElement : ElementBase
    {
        private string value;
        private string toolTip;

        public StringElement(string key, string displayName, string value) : base(key, displayName)
        {
            this.value = value;
        }

        public string Value
        {
            get => value; 
            set
            {
                SetProperty(ref this.value, value);
                IsDirty = true;
            }
        }
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }

        public override UserControl GetView()
        {
            return new StringControl(this);
        }

        public override object GetValue() => Value;
    }
}