﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для LabelControl.xaml
    /// </summary>
    public partial class LabelControl : UserControl
    {
        internal LabelControl(LabelElement model)
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
