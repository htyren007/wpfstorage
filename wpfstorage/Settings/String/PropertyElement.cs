﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    public class PropertyElement : ElementBase
    {
        private string value;
        private string toolTip;

        public PropertyElement(string displayName, string value) : base("", displayName)
        {
            Value = value;
        }

        public string Value
        {
            get => value; 
            set
            {
                SetProperty(ref this.value, value);
                IsDirty = true;
            }
        }
        public string ToolTip { get => toolTip; set => SetProperty(ref toolTip, value); }

        public override object GetValue() => null;

        public override UserControl GetView()
        {
            return new PropertyControl() { DataContext = this };
        }
    }
}