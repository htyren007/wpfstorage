﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    internal class LabelElement : ElementBase
    {
        public LabelElement(string label): base("", label)
        {
        }

        public string ToolTip { get; set; }

        public override object GetValue()
        {
            return null;
        }

        public override UserControl GetView()
        {
            return new LabelControl(this);
        }
    }
}