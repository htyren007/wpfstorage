﻿using System.Windows.Controls;

namespace WPFStorage.Settings
{
    /// <summary>
    /// Логика взаимодействия для StringControl.xaml
    /// </summary>
    public partial class StringControl : UserControl
    {
        public StringControl(StringElement model)
        {
            InitializeComponent();
            DataContext = model;
        }
    }
}
