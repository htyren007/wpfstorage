﻿using System;

namespace WPFStorage.Settings
{
    public class StringConfigure
    {
        private StringElement element;

        public StringConfigure(StringElement element)
        {
            this.element = element;
        }

        public StringConfigure SetToolTip(string toolTip)
        {
            element.ToolTip = toolTip;
            return this;
        }

        public StringConfigure SetValue(string value)
        {
            element.Value = value;
            return this;
        }

        public StringConfigure OnChange(Action<string> onValueChange)
        {
            element.PropertyChanged += (a, e) => {
                if (e.PropertyName == nameof(element.Value))
                {
                    onValueChange(element.Value);
                }
            };
            return this;
        }
    }
}