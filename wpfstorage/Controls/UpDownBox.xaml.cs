﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;

namespace WPFStorage.Controls
{
    /// <summary>
    /// Логика взаимодействия для UpDownControl.xaml
    /// </summary>
    public partial class UpDownBox : UserControl
    {
        public static readonly DependencyProperty ValueProperty;
        public static readonly DependencyProperty InkermenterProperty;

        static UpDownBox()
        {
            ValueProperty = DependencyProperty.Register(
                        nameof(Value),
                        typeof(double),
                        typeof(UpDownBox),
                        new FrameworkPropertyMetadata(
                            0d,
                            FrameworkPropertyMetadataOptions.AffectsMeasure 
                            | FrameworkPropertyMetadataOptions.AffectsRender, OnValueChange));
            InkermenterProperty = DependencyProperty.Register(
                       "Inkermenter",
                       typeof(double),
                       typeof(UpDownBox),
                       new FrameworkPropertyMetadata(
                           1d,
                           FrameworkPropertyMetadataOptions.AffectsMeasure |
                           FrameworkPropertyMetadataOptions.AffectsRender));
        }

        private static void OnValueChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is UpDownBox box && e.NewValue is double nv)
            {
                box.Value = nv;
            }
        }

        private DispatcherTimer timer;

        public double Inkermenter
        {
            get { return (double)GetValue(InkermenterProperty); }
            set { SetValue(InkermenterProperty, value); }
        }
        public double Value
        {
            get 
            { 
                return (double)GetValue(ValueProperty); 
            }
            set 
            { 
                SetValue(ValueProperty, value);
            }
        }

        public UpDownBox()
        {
            InitializeComponent();
            if (timer == null)
            {
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            }
        }

        private void Up_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            timer.Stop();
            timer.Tick -= TickUp;
            Amount(+Inkermenter);
        }

        private void Up_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            timer.Tick += TickUp;
            timer.Start();

        }

        private void Down_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            timer.Stop();
            timer.Tick -= TickDown;
            Amount(-Inkermenter);
        }

        private void Down_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            timer.Tick += TickDown;
            timer.Start();
        }

        private void TickUp(object sender, EventArgs e) => Amount(+Inkermenter);

        private void TickDown(object sender, EventArgs e) => Amount(-Inkermenter);

        private void input_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            Amount((e.Delta > 0 ? 1 : -1) * Inkermenter);
        }

        private void Amount(double v)
        {
            Value += v;
        }
    }
}
