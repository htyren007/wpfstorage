﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFStorage.Controls.ColorInput
{
    /// <summary>
    /// Логика взаимодействия для ColorPicker.xaml
    /// </summary>
    public partial class ColorPicker : UserControl
    {
        #region static
        public static readonly DependencyProperty ColorProperty;
        public static readonly DependencyProperty RProperty;
        public static readonly DependencyProperty GProperty;
        public static readonly DependencyProperty BProperty;

        static ColorPicker()
        {
            ColorProperty = DependencyProperty.Register(
                                nameof(Color),
                                typeof(Color),
                                typeof(ColorPicker),
                                new FrameworkPropertyMetadata(
                                    defaultValue: Colors.Green,
                                    flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                    propertyChangedCallback: OnColorChanged));

            RProperty = DependencyProperty.Register(
                                nameof(R),
                                typeof(byte),
                                typeof(ColorPicker),
                                new FrameworkPropertyMetadata(
                                    defaultValue: (byte)255,
                                    flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                    propertyChangedCallback: (d, e) => OnComponentsChanged(d, e, (c, r) => new Color() { R = r, B = c.B, G = c.G, A = c.A })));

            GProperty = DependencyProperty.Register(
                                nameof(G),
                                typeof(byte),
                                typeof(ColorPicker),
                                new FrameworkPropertyMetadata(
                                    defaultValue: (byte)0,
                                    flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                    propertyChangedCallback: (d, e) => OnComponentsChanged(d, e, (c, g) => new Color() { R = c.R, B = c.B, G = g, A = c.A })));
            BProperty = DependencyProperty.Register(
                                nameof(B),
                                typeof(byte),
                                typeof(ColorPicker),
                                new FrameworkPropertyMetadata(
                                    defaultValue: (byte)0,
                                    flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                    propertyChangedCallback: (d, e) => OnComponentsChanged(d, e, (c, b) => new Color() { R = c.R, B = b, G = c.G, A = c.A })));
        }
        #endregion

        private byte stopNotify;
        private Color selecledColor;
        public ColorPicker()
        {
            InitializeComponent();
            spectrumBox.ColorChanged += SpectrumBox_ColorChanged;
            rectangle.ColorChanged += Rectangle_ColorChanged;
            //Loaded += ColorPicker_Loaded;
        }

        #region property
        public byte R
        {
            get
            {
                //return Color.R;
                return (byte)GetValue(RProperty);
            }

            set
            {
                SetValue(RProperty, value);
            }
        }

        public byte B
        {
            get => (byte)GetValue(BProperty);
            set => SetValue(BProperty, value);
        }

        public byte G
        {
            get => (byte)GetValue(GProperty);
            set => SetValue(GProperty, value);
        }

        public Color Color
        {
            get
            {
                return (Color)GetValue(ColorProperty);

                //return selecledColor;
            }

            set
            {
                SetValue(ColorProperty, value);
            }
        }
        #endregion

        public event Action<Color> ColorChanged;

        #region private methods
        private void ColorPicker_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ColorPicker_Loaded;
            spectrumBox.Color = Color;
            rectangle.Color = Color;
        }

        private static void OnComponentsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e, Func<Color, byte, Color> update)
        {
            if (d is ColorPicker control && e.NewValue is byte component)
            {
                var c = update(control.Color, component);
                control.Color = c;
            }
        }

        private static void OnColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ColorPicker control && e.NewValue is Color color)
            {
                control.Color = color;
                if (control.stopNotify == 0)
                {
                    control.stopNotify++;
                    control.spectrumBox.Color = color;
                    control.rectangle.Color = color;
                    control.R = color.R;
                    control.G = color.G;
                    control.B = color.B;
                    control.stopNotify--;
                }
            }
        }

        private void Rectangle_ColorChanged(Color obj)
        {
            selecledColor = obj;
            // stopNotify++;
            Color = obj;
            // stopNotify--;

            ColorChanged?.Invoke(obj);
        }

        private void SpectrumBox_ColorChanged(Color obj)
        {
            if (rectangle.TargetColor != obj)
                rectangle.TargetColor = obj;
        }
        #endregion
    }
}
