﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace WPFStorage.Controls.ColorInput
{
    /// <summary>
    /// Логика взаимодействия для SpectrumBox.xaml
    /// </summary>
    public partial class SpectrumBox : UserControl
    {
        public static readonly DependencyProperty ColorProperty;

        private bool isPress;
        public event Action<Color> ColorChanged;

        static SpectrumBox()
        {
            ColorProperty = DependencyProperty.Register(
                            nameof(Color),
                            typeof(Color),
                            typeof(SpectrumBox),
                            new FrameworkPropertyMetadata(
                                defaultValue: Colors.Green,
                                flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, 
                                propertyChangedCallback: OnColorChanged));
        }

        private static void OnColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SpectrumBox spectrum && e.NewValue is Color color)
            {
                spectrum.UpdatePoint(color);
            }
        }

        public SpectrumBox()
        {
            InitializeComponent();
            CreateSpectrim();

            Loaded += SpectrumBox_Loaded;

        }


        public Color Color
        {
            get => (Color)GetValue(ColorProperty);
            set
            {
                if ((Color)GetValue(ColorProperty) != value)
                {
                    SetValue(ColorProperty, value);
                    ColorChanged?.Invoke(value);
                }
            }
        }

        public HsvColor Hsv { get; private set; }

        private void SpectrumBox_Loaded(object sender, RoutedEventArgs e) => UpdatePoint(Color);

        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPress)
            {
                var p = e.GetPosition(sender as IInputElement);
                UpdatePoint(p.X);
                UpdateColor(p);
            }
        }

        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) => isPress = false;

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isPress = true;
            var p = e.GetPosition(sender as IInputElement);
            UpdatePoint(p.X);
            UpdateColor(p);

        }

        private void UpdateColor(Point point)
        {
            try
            {
                //var renderTargetBitmap = new RenderTargetBitmap((int)spectr.ActualWidth,
                //                                                                (int)spectr.ActualHeight,
                //                                                                96, 96, PixelFormats.Default);
                //renderTargetBitmap.Render(spectr);

                //var croppedBitmap = new CroppedBitmap(renderTargetBitmap,
                //                                      new Int32Rect((int)point.X, (int)point.Y, 1, 1));

                //var pixels = new byte[4];
                //croppedBitmap.CopyPixels(pixels, 4, 0);

                //// Assign the sampled color to a SolidColorBrush and return as conversion.
                //Color = Color.FromArgb(255, pixels[2], pixels[1], pixels[0]);

                var hsv = new HsvColor(1,1,1); // ColorHelper.ToHsv(Color);
                hsv.H = point.X * 360 / ActualWidth;
                //hsv.S = 1;
                //hsv.V = 0.9;

                Color = ColorHelper.ToRbg(hsv);
            }
            catch
            {
                Color = Colors.Red;
            }
        }

        private void UpdatePoint(Color color)
        {
            var hsv = ColorHelper.ToHsv(color);
            double x = 0;

            Hsv = hsv;

            x = hsv.H * ActualWidth / 360;

            UpdatePoint(x);
        }

        private void UpdatePoint(double x) => Canvas.SetLeft(TargerRect, x - 5);

        private void CreateSpectrim()
        {
            const int CountStep = 30;
            var step = (double)1 / CountStep;
            var stepH = (double)360 / CountStep;

            for (int i = 0; i < CountStep - 1; i++)
            {
                var color = ColorHelper.ConvertHsvToRgb(i * stepH, 1, 1);
                Addcolor(color, i * step);
            }


            Addcolor(ColorHelper.ConvertHsvToRgb(0, 1, 1), 1);

            void Addcolor(Color _color, double offset)
            {
                var stop = new GradientStop(_color, offset);
                mainSpectrum.GradientStops.Add(stop);
            }
        }
    }
}
