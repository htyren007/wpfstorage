﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPFStorage.Controls.ColorInput
{
    /// <summary>
    /// Логика взаимодействия для SpectrumRectangle.xaml
    /// </summary>
    public partial class SpectrumRectangle : UserControl
    {
        public static readonly DependencyProperty ColorProperty;
        public static readonly DependencyProperty TargetColorProperty;


        private bool isPress;

        static SpectrumRectangle()
        {
            ColorProperty = DependencyProperty.Register(
                            nameof(Color),
                            typeof(Color),
                            typeof(SpectrumRectangle),
                            new FrameworkPropertyMetadata(
                                defaultValue: Colors.White,
                                flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                propertyChangedCallback: OnColorChanged));

            TargetColorProperty = DependencyProperty.Register(
                            nameof(TargetColor),
                            typeof(Color),
                            typeof(SpectrumRectangle),
                            new FrameworkPropertyMetadata(
                                defaultValue: Colors.White,
                                flags: FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender,
                                propertyChangedCallback: OnTargetColorChanged));
        }


        public SpectrumRectangle()
        {
            InitializeComponent();
            //Loaded += SpectrumRectangle_Loaded;
            //IsVisibleChanged += SpectrumRectangle_IsVisibleChanged;
            SizeChanged += SpectrumRectangle_SizeChanged;
        }

        public event Action<Color> ColorChanged;


        public Color TargetColor
        {
            get => (Color)GetValue(TargetColorProperty);
            set => SetValue(TargetColorProperty, value);
        }
        public Color Color
        {
            get => (Color)GetValue(ColorProperty);
            set
            {
                if ((Color)GetValue(ColorProperty) != value)
                {
                    SetValue(ColorProperty, value);
                    ColorChanged?.Invoke(value);
                }
            }
        }

        private void SpectrumRectangle_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdatePoint(Color);
        }

        private static void OnColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SpectrumRectangle spectrum && e.NewValue is Color color)
            {
                spectrum.UpdatePoint(color);
            }
        }

        private static void OnTargetColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SpectrumRectangle spectrum && e.NewValue is Color color)
            {
                var x = Canvas.GetLeft(spectrum.TargerRect)+5;
                var y = Canvas.GetTop(spectrum.TargerRect)+5;
                
                spectrum.UpdateColor(new Point(x, y));
            }
        }

        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPress)
            {
                var p = e.GetPosition(sender as IInputElement);
                UpdatePoint(p);
                UpdateColor(p);
            }
        }

        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isPress = false;
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isPress = true;
            var p = e.GetPosition(sender as IInputElement);
            UpdatePoint(p);
            UpdateColor(p);

        }

        private void UpdatePoint(Color color)
        {
            HsvColor hsv = ColorHelper.ToHsv(color);
            //HsvColor hsvTarget = new HsvColor(hsv.H, 1, 1);


            double x = hsv.S * ActualWidth; 
            double y = (1 - hsv.V) * ActualHeight;

            Point p = new Point(x, y);

            UpdatePoint(p);

            //TargetColor = ColorHelper.ToRbg(hsvTarget);
        }

        private void UpdatePoint(Point p)
        {
            Canvas.SetLeft(TargerRect, p.X - 5);
            Canvas.SetTop(TargerRect, p.Y - 5);
        }

        private void UpdateColor(Point point)
        {
            try
            {
                HsvColor hsv = ColorHelper.ToHsv(TargetColor);

                hsv.S = point.X / ActualWidth;
                hsv.V = 1 - (point.Y / ActualHeight);

                Color = ColorHelper.ToRbg(hsv);

                //var renderTargetBitmap = new RenderTargetBitmap((int)spectr.ActualWidth,
                //                                                                (int)spectr.ActualHeight,
                //                                                                96, 96, PixelFormats.Default);
                //renderTargetBitmap.Render(spectr);

                //var croppedBitmap = new CroppedBitmap(renderTargetBitmap,
                //                                      new Int32Rect((int)point.X, (int)point.Y, 1, 1));

                //var pixels = new byte[4];
                //croppedBitmap.CopyPixels(pixels, 4, 0);

                //// Assign the sampled color to a SolidColorBrush and return as conversion.
                //Color = Color.FromArgb(255, pixels[2], pixels[1], pixels[0]);
            }
            catch
            {
                Color = Colors.White;
            }
        }
    }
}
