﻿using System;
using System.Windows.Media;

namespace WPFStorage.Controls.ColorInput
{

    public static class ColorHelper
    {
        public static readonly Color DEFAULT_COLOR = Colors.Black;

        public static Color ToColor(string stringColor)
        {
            if (string.IsNullOrWhiteSpace(stringColor))
                return DEFAULT_COLOR;
            var color = (Color)ColorConverter.ConvertFromString(stringColor);
            return color;
        }

        /// <summary>
        /// Converts an RGB color to an HSV color.
        /// </summary>
        public static HsvColor ConvertRgbToHsv(int r, int b, int g)
        {
            double delta, min;
            double h = 0, s, v;

            min = Math.Min(Math.Min(r, g), b);
            v = Math.Max(Math.Max(r, g), b);
            delta = v - min;

            s = v == 0.0 ? 0 : delta / v;

            if (s == 0)
                h = 0.0;
            else
            {
                if (r == v)
                    h = (g - b) / delta;
                else if (g == v)
                    h = 2 + (b - r) / delta;
                else if (b == v)
                    h = 4 + (r - g) / delta;

                h *= 60;
                if (h < 0.0)
                    h += 360;
            }

            HsvColor hsvColor = new HsvColor
            {
                H = h,
                S = s,
                V = v / 255
            };

            return hsvColor;
        }

        /// <summary>
        /// Converts an HSV color to an RGB color.
        /// </summary>
        public static Color ConvertHsvToRgb(double h, double s, double v)
        {
            double r, g, b;

            if (s == 0)
                r = g = b = v;
            else
            {
                int i;
                double f, p, q, t;

                h = h == 360 ? 0 : h / 60;

                i = (int)Math.Truncate(h);
                f = h - i;

                p = v * (1.0 - s);
                q = v * (1.0 - (s * f));
                t = v * (1.0 - (s * (1.0 - f)));

                switch (i)
                {
                    case 0:
                        r = v;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = v;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = v;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = v;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = v;
                        break;
                    default:
                        r = v;
                        g = p;
                        b = q;
                        break;
                }
            }

            return Color.FromArgb(255, (byte)(r * 255), (byte)(g * 255), (byte)(b * 255));

        }

        public static HsvColor ToHsv(Color value)
        {
            var hsv = ConvertRgbToHsv(value.R, value.B, value.G);
            hsv.Alpha = value.A / 255.0d;
            return hsv;
        }

        public static Color ToRbg(HsvColor value)
        {
            var rbg = ConvertHsvToRgb(value.H, value.S, value.V);
            rbg.A = (byte)(value.Alpha * 255);
            return rbg;
        }
    }
}
