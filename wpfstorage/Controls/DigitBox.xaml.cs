﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WPFStorage.Controls
{
    /// <summary>
    /// Логика взаимодействия для DigitBox.xaml
    /// </summary>
    public partial class DigitBox : UserControl
    {
        #region Static
        public static readonly DependencyProperty ValueProperty;

        public static readonly DependencyProperty IsIntegerNumericProperty;

        static DigitBox()
        {
            ValueProperty = DependencyProperty.Register(
                            nameof(Value),
                            typeof(double),
                            typeof(DigitBox),
                            new FrameworkPropertyMetadata(ValueCallbackChange, ValueCoerceCallback));
            IsIntegerNumericProperty = DependencyProperty.Register(
                            nameof(IsIntegerNumeric),
                            typeof(bool),
                            typeof(DigitBox),
                            new FrameworkPropertyMetadata(
                                false,
                                FrameworkPropertyMetadataOptions.AffectsMeasure |
                                FrameworkPropertyMetadataOptions.AffectsRender));
        }

        private static void ValueCallbackChange(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            if (d is DigitBox box && args.NewValue is double digit)
            {
                box.Value = digit;
            }
        }

        private static object ValueCoerceCallback(DependencyObject d, object baseValue)
        {
            if (d is DigitBox box && baseValue is float digit)
            {
                box.Value = digit;
            };
            return baseValue;
        }

        public static int DecimalSeparatorCount(string s)
        {
            string substr = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
            int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
            return count;
        } 
        #endregion
        private bool canEditText = true;

        public DigitBox()
        {
            InitializeComponent();
            digit.Text = Value.ToString();
        }

        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set
            {
                SetValue(ValueProperty, value);
                if (canEditText)
                    digit.Text = value.ToString();
            }
        }

        public bool IsIntegerNumeric
        {
            get => (bool)GetValue(IsIntegerNumericProperty);
            set => SetValue(IsIntegerNumericProperty, value);
        }

        private void digit_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool isDigit = Char.IsDigit(e.Text, 0);

            bool isSeparator = (!IsIntegerNumeric) 
                && e.Text == CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()
                && DecimalSeparatorCount(((TextBox)sender).Text) < 1;

                e.Handled = !isDigit && !isSeparator;
        }

        private void digit_TextChanged(object sender, TextChangedEventArgs e)
        {
            canEditText = false;
            if (IsIntegerNumeric && int.TryParse(digit.Text, out int num))
                Value = num;
            else if (double.TryParse(digit.Text, NumberStyles.Any, CultureInfo.CurrentCulture, out double dig))
            {
                if (Value != dig)
                    Value = dig;
            }
            canEditText = true;
        }
    }
}
