﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFStorage.Controls
{
    /// <summary>
    /// Логика взаимодействия для ColorDropDown.xaml
    /// </summary>
    public partial class ColorDropDown : UserControl
    {

        #region DependencyProperty
        public static readonly DependencyProperty IsEditProperty;
        public static readonly DependencyProperty SelectedProperty;

        static ColorDropDown()
        {
            IsEditProperty = DependencyProperty.Register(
                                nameof(IsEdit),
                                typeof(bool),
                                typeof(ColorDropDown),
                                new FrameworkPropertyMetadata(
                                    false,
                                    FrameworkPropertyMetadataOptions.AffectsMeasure |
                                    FrameworkPropertyMetadataOptions.AffectsRender));

            SelectedProperty = DependencyProperty.Register(
                                nameof(Selected),
                                typeof(Color),
                                typeof(ColorDropDown),
                                new FrameworkPropertyMetadata(
                                    Colors.Red,
                                    FrameworkPropertyMetadataOptions.AffectsMeasure |
                                    FrameworkPropertyMetadataOptions.AffectsRender));
        }

        public Color Selected
        {
            get => (Color)GetValue(SelectedProperty);
            set => SetValue(SelectedProperty, value);
        }

        public bool IsEdit
        {
            get => (bool)GetValue(IsEditProperty);
            set => SetValue(IsEditProperty, value);
        }

        #endregion

        public ColorDropDown()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsEdit = !IsEdit;
        }
    }
}
