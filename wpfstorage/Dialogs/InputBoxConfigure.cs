﻿using System.Windows;

namespace WPFStorage.Dialogs
{
    public class InputBoxConfigure : ConfigureBase<InputBoxModel, InputBoxConfigure>
    {

        public InputBoxConfigure()
        {
            model = new InputBoxModel();
            model.InputIsActive = true;
            model.ButtonsAlignment = System.Windows.HorizontalAlignment.Center;
        }

        /// <summary>
        /// Открыть диалог, паток будет приастоновлен до закрытия окна
        /// </summary>
        /// <param name="value"></param>
        /// <returns>true - нажата кнопка Ok, false - диалог закрыт другими способами</returns>
        public bool ShowDialog(out string value)
        {
            WindowBox window = new WindowBox(model);
            bool? res = window.ShowDialog();
            value =  model.Input;
            return res == true;

        }

        
        public override void Show()
        {
            WindowBox window = new WindowBox(model);
            window.ShowDialog();
        }

        
        public override void ShowAsunc()
        {
            WindowBox window = new WindowBox(model);
            window.Show();
        }

        public InputBoxConfigure Timeout(long value)
        {
            model.Timeout = value;
            return this;
        }

        public InputBoxConfigure Value(string value)
        {
            model.Input = value;
            return this;
        }

        public InputBoxConfigure OkIsActive(bool value)
        {
            model.OkIsActive = value;
            return this;
        }

        internal InputBoxConfigure ButtonsAlignment(HorizontalAlignment value)
        {
            model.ButtonsAlignment = value;
            return this;
        }

        public InputBoxConfigure CancelIsActive(bool value)
        {
            model.CancelIsActive = value;
            return this;
        }

        public InputBoxConfigure InputIsActive(bool value)
        {
            model.InputIsActive = value;
            return this;
        }
    }
}