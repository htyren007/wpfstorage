﻿namespace WPFStorage.Dialogs.Json
{
    internal class JsonPreviewModel : ADialogModelBase
    {
        private double height = 250;
        private double width = 400;
        private string text;
        private bool isShowText = false;

        public JsonPreviewModel(): base()
        {
        }

        public double Height { get => height; set => SetProperty(ref height, value); }
        public double Width { get => width; set => SetProperty(ref width, value); }
        public string Text { get => text; set => SetProperty(ref text, value); }
        public bool TextIsActive { get => isShowText; set => SetProperty(ref isShowText, value); }
        public string Json { get; internal set; }

        protected override void Cancel()
        {
            
        }

        protected override void Ok()
        {
        }
    }
}
