﻿using System;

namespace WPFStorage.Dialogs.Json
{
    public class JsonPreviewConfigure 
    {
        private JsonPreviewModel model;

        public JsonPreviewConfigure()
        {
            this.model = new JsonPreviewModel();
            model.Text = "Просмотр Json";
        }

        public JsonPreviewConfigure Json(string json)
        {
            model.Json = json;
            return this;
        }

        public JsonPreviewConfigure OkText(string value)
        {
            model.OkText = value;
            return this;
        }

        public JsonPreviewConfigure OkIsActive(bool value)
        {
            model.OkIsActive = value;
            return this;
        }

        public JsonPreviewConfigure CancelText(string value)
        {
            model.CancelText = value;
            return this;
        }

        public JsonPreviewConfigure CancelIsActive(bool value)
        {
            model.CancelIsActive = value;
            return this;
        }

        public JsonPreviewConfigure Width(double width)
        {
            model.Width = width;
            return this;
        }

        public JsonPreviewConfigure Height(double heigth)
        {
            model.Height = heigth;
            return this;
        }

        public JsonPreviewConfigure Title(string title)
        {
            model.Title = title;
            return this;
        }

        public JsonPreviewConfigure Text(string text)
        {
            model.Text = text;
            model.TextIsActive = !string.IsNullOrWhiteSpace(text);
            return this;
        }

        public void Open()
        {
            JsonPreviewWindow window = CreateWindow();
            window.Show();
        }

        public bool OpenDialog()
        {
            JsonPreviewWindow window = CreateWindow();
            window.ShowDialog();
            return model.PressOk;
        }

        private JsonPreviewWindow CreateWindow()
        {
            JsonPreviewWindow window = new JsonPreviewWindow();
            window.DataContext = model;
            window.Width = model.Width;
            window.Height = model.Height;
            window.JSON.Load(model.Json);
            model.CloseWindow = () => window.Close();

            return window;
        }
    }
}
