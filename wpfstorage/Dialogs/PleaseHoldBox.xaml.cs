﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для PleaseHold.xaml
    /// </summary>
    public partial class PleaseHoldBox : Window
    {
        public PleaseHoldBox(PleaseHoldModel model)
        {
            InitializeComponent();
            DataContext = model;
            model.CloseHandler += Model_CloseHandler;
        }

        private void Model_CloseHandler()
        {
            this.Close();
        }
    }

    public class PleaseHoldModel : ObservableObject
    {
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public int Maximum { get => maximum; set => SetProperty(ref maximum, value); }
        public int Minimum { get => minimum; set => SetProperty(ref minimum, value); }
        public int Progress { get => progress; set => SetProperty(ref progress, value); }

        public bool IsStopped { get => isStopped; private set => SetProperty(ref isStopped, value); }
        public RelayCommand CancelCommand { get; }

        public event Action CancelHandler;
        internal event Action CloseHandler;
        private string cancelText;
        private string message;
        private int maximum;
        private int minimum;
        private int progress;
        private bool isStopped;

        public PleaseHoldModel()
        {
            CancelCommand = new RelayCommand(Cancel);
        }

        public void Cancel()
        {
            IsStopped = true;
            CancelHandler?.Invoke();
        }

        public void Close()
        {
            CloseHandler?.Invoke();
        }
    }
}
