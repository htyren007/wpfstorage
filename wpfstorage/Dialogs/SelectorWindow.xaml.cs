﻿using System.Windows;

namespace WPFStorage.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SelectorWindow.xaml
    /// </summary>
    public partial class SelectorWindow : Window
    {
        public SelectorWindow(SelectorViewModel model)
        {
            InitializeComponent();
            Closing += (e, o) =>
            {
                DialogResult = model.Select != null;
            };
            model.Close += () => Close();
            DataContext = model;
            Loaded += (a, b) =>
            {
                model.Initialize();
            };
        }
    }
}
