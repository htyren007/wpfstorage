﻿using System;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    public abstract class ADialogModelBase : ObservableObject
    {
        #region fields
        private string title = "Окно ввода";
        private string okText = "Ок";
        private string cancelText = "Отмена";
        private bool okIsActive = true;
        private bool cancelIsActive = true; 
        #endregion

        #region constructor
        public ADialogModelBase()
        {
            OkCommand = new RelayCommand(OkMethod);
            CancelCommand = new RelayCommand(CancelMethod);
        } 
        #endregion

        #region properties
        /// <summary>
        /// Название окна
        /// </summary>
        public string Title { get => title; set => SetProperty(ref title, value); }

        /// <summary>
        /// Текст кнопки Ok
        /// </summary>
        public string OkText { get => okText; set => SetProperty(ref okText, value); }
        /// <summary>
        /// Текст кнопки Cancel
        /// </summary>
        public string CancelText { get => cancelText; set => SetProperty(ref cancelText, value); }

        public bool PressOk { get; internal set; } = false;
        /// <summary>
        /// Видимость кнопки Ok
        /// </summary>
        public bool OkIsActive { get => okIsActive; set => SetProperty(ref okIsActive, value); }
        /// <summary>
        /// Видимость кнопки Cancel
        /// </summary>
        public bool CancelIsActive { get => cancelIsActive; set => SetProperty(ref cancelIsActive, value); }

        public RelayCommand OkCommand { get; }
        public RelayCommand CancelCommand { get; }

        public Action CloseWindow { get; internal set; }
        #endregion

        #region methods
        private void OkMethod()
        {
            PressOk = true;
            Ok();
            CloseWindow?.Invoke();
        }

        private void CancelMethod()
        {
            Cancel();
            CloseWindow?.Invoke();
        }

        protected abstract void Ok();
        protected abstract void Cancel(); 
        #endregion
    }
}