﻿using System;
using System.Windows;
using System.Windows.Threading;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    public class BoxModelBase : ADialogModelBase
    {
        #region Field
        private string question = "Введите текст";
        private HorizontalAlignment questionAlignment;
        private HorizontalAlignment buttonsAlignment;
        private DispatcherTimer timer;
        private long timeout;
        private bool inputIsActive;
        #endregion

        #region Constructor
        public BoxModelBase(): base()
        {
        }
        #endregion

        #region Binding
        public string Question
        {
            get => question;
            set => SetProperty(ref question, value);
        }

        public HorizontalAlignment QuestionAlignment
        {
            get => questionAlignment;
            set => SetProperty(ref questionAlignment, value);
        }

        public HorizontalAlignment ButtonsAlignment
        {
            get => buttonsAlignment;
            set => SetProperty(ref buttonsAlignment, value);
        }

        public long Timeout
        {
            get => timeout;
            set => SetProperty(ref timeout, value);
        }

        public bool InputIsActive
        {
            get => inputIsActive;
            set => SetProperty(ref inputIsActive, value);
        }
        #endregion

        /// <summary>Нажание на кнопку отмены</summary>
        public Action Close { get; internal set; }

        /// <summary>Нажание на кнопку ok</summary>
        public Action Success { get; internal set; }

        /// <summary>Инициализация таймера</summary>
        public virtual void Initialize()
        {
            if (Timeout > 0)
            {
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(Timeout * TimeSpan.TicksPerMillisecond);
                timer.Tick += TimerTick;
                timer.Start();
            }
        }

        protected override void Cancel() => Close?.Invoke();

        protected override void Ok() => Success?.Invoke();

        private void TimerTick(object sender, EventArgs e)
        {
            timer.Stop();
            Close?.Invoke();
        }
    }
}