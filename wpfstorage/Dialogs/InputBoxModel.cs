﻿namespace WPFStorage.Dialogs
{
    public class InputBoxModel : BoxModelBase
    {
        #region Field
        private string input;
        #endregion

        #region Constructor
        public InputBoxModel() : base()
        {
            
        }

        #endregion
        #region Binding

        public string Input
        {
            get => input;
            set => SetProperty(ref input, value);
        }

        #endregion
    }
}
