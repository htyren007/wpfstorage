﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using WPFStorage.Dialogs.Json;

namespace WPFStorage.Dialogs
{
    /// <summary>
    /// Простые окошли для взаимодействия с пользователем
    /// </summary>
    public static class WinBox
    {
        /// <summary>
        /// Окно сообщения пользователю, может закрытся через <c>timeout</c> миллисекунд 
        /// </summary>
        /// <param name="message">Текст cообщение</param>
        /// <param name="title">Текст заголовка окна</param>
        /// <param name="timeout">Таймаут закрытия в миллисекундах</param>
        public static void ShowMessage(string message, string title = "Message", long timeout = 0)
        {
            
            var box = Input().Question(message).Title(title)
                .InputIsActive(false)
                .CancelIsActive(false)
                .ButtonsAlignment(HorizontalAlignment.Center);
            if (timeout != 0)
                box.Timeout(timeout);
            box.Show();
        }

        /// <summary>
        /// Окно ввода строки
        /// </summary>
        /// <param name="question">Вопрос пользователю или объяснение что о должен ввести</param>
        /// <param name="title">Заголовок окна</param>
        /// <param name="defautValue">Значение по умолчанию</param>
        /// <returns>Введёная строка или null</returns>
        public static string ShowInput(string question, string title = "Input", string defautValue = "")
        {
            var box = Input().Question(question).Title(title).Value(defautValue).InputIsActive(true);
            if (box.ShowDialog(out string result))
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// Окно вопроса с двумя кнопками выбора.
        /// </summary>
        /// <param name="question">Текст вопроса</param>
        /// <param name="title">Заголовок окна</param>
        /// <param name="okText">Тест на кнопке подтверждения</param>
        /// <param name="cancelText">Текст на кнопке отмены</param>
        /// <returns></returns>
        public static bool ShowQuestion(string question, string title = "Question", string okText = "Yes", string cancelText = "No")
        {
            var box = Input().Question(question).Title(title).ButtonsAlignment(HorizontalAlignment.Center)
                .OkText(okText).OkIsActive(true)
                .CancelText(cancelText).CancelIsActive(true)
                .InputIsActive(false);
            box.Show();
            return box.GetModel().PressOk;
        }


        /// <summary>
        /// Инструмент для ввода текста
        /// </summary>
        /// <returns></returns>
        public static InputBoxConfigure Input()
        {
            return new InputBoxConfigure();
        }

        /// <summary>
        /// Инструмент для отображения больших текстов
        /// </summary>
        public static JsonPreviewConfigure JsonPreview()
        {
            return new JsonPreviewConfigure();
        }

        /// <summary>
        /// Инструмент для отображения больших текстов
        /// </summary>
        public static LargeTextConfigure LargeText()
        {
            return new LargeTextConfigure();
        }

        /// <summary>
        /// Окно пользователю, для вывора числа с помощью слайдера
        /// </summary>
        /// <returns></returns>
        public static SliderDialogConfigure Slider()
        {
            return new SliderDialogConfigure();
        }

        public static DigitBoxConfigure DigitBox()
        {
            return new DigitBoxConfigure();
        }

        ///// <summary>
        ///// Окно с большим количеством кнопок надписи котороых переданы в <see cref="collect"/>
        ///// </summary>
        ///// <param name="collect">коллекция надписей на кнопках</param>
        ///// <param name="question">текст вопроса</param>
        ///// <param name="title">заголовок окна</param>
        ///// <param name="timeout">таймаут по истечению которого окно закроется</param>
        ///// <returns>надпись на кнопке которую нажали</returns>
        //public static string SelectorBox(
        //    IEnumerable<string> collect,
        //    string question = "Выберите один из вариантов:",
        //    string title = "Окно выбора",
        //    int timeout = 0)
        //{


        //    //SelectorViewModel model = new SelectorViewModel();
        //    //SetQuestionTitleTextOkCansel(model, question, title, null, null);
        //    //model.Items = new List<string>(collect);
        //    //model.Timeout = timeout;
        //    //ShowSelectorBoxModel(model);
        //    //return model.Select;
        //}

        public static SelectorConfigure Selector()
        {
            return new SelectorConfigure();
        }


        public static PleaseHoldModel PleaseHold(string message, string cancelText = "Cancel")
        {
            PleaseHoldModel model = new PleaseHoldModel();
            model.Message = message;
            model.CancelText = cancelText;

            PleaseHoldBox window = new PleaseHoldBox(model);
            window.Show();

            return model;
        }

        private static void SetQuestionTitleTextOkCansel(BoxModelBase model, string question, string title, string okText, string cancelText)
        {
            model.Question = question;
            if (!string.IsNullOrWhiteSpace(title)) model.Title = title;
            if (!string.IsNullOrWhiteSpace(okText)) model.OkText = okText;
            if (!string.IsNullOrWhiteSpace(cancelText)) model.CancelText = cancelText;
        }

        internal static void ShowNumericBoxModel(DigitBoxModel model)
        {
            DigitBox window = new DigitBox(model);
            bool? res = window.ShowDialog();
            if (res == true)
            {
                model.PressOk = true;
            }
        }



        private static void ShowInputBoxModel(InputBoxModel model)
        {
            WindowBox window = new WindowBox(model);
            bool? res = window.ShowDialog();
            if (res == true)
            {
                model.PressOk = true;
            }
        }
    }
}
