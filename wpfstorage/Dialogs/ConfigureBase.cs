﻿namespace WPFStorage.Dialogs
{
    public abstract class ConfigureBase<TModel, TConfigure> : IConfigureBase 
        where TModel : BoxModelBase 
        where TConfigure : IConfigureBase
    {
        protected TModel model;

        public TConfigure CancelText(string cancelText)
        {
            model.CancelText = cancelText;
            if (this is TConfigure configure)
                return configure;
            return default;
        }

        public TConfigure OkText(string okText)
        {
            model.OkText = okText;
            if (this is TConfigure configure)
                return configure;
            return default;
        }

        public TConfigure Question(string question)
        {
            model.Question = question;
            if (this is TConfigure configure)
                return configure;
            return default;
        }

        public abstract void Show();

        public abstract void ShowAsunc();

        public TConfigure Title(string title)
        {
            model.Title = title;
            if (this is TConfigure configure)
                return configure;
            return default;
        }

        internal TModel GetModel()
        {
            return model;
        }
    }

    public interface IConfigureBase
    {
        /// <summary>
        /// Открыть диалог, паток будет приастоновлен до закрытия окна
        /// </summary>
        void Show();

        /// <summary>
        /// Открыть окно
        /// </summary>
        void ShowAsunc();
    }
}