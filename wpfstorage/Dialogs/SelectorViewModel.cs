﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    public class SelectorViewModel : BoxModelBase
    {
        #region Field
        private List<string> items = new List<string>();
        private double panelWidth = 300;
        #endregion

        #region Constructor
        public SelectorViewModel():base()
        {
            SelectItem = new RelayCommand<string>(SelectItemMethod);
        }
        #endregion

        #region Binding
        public List<string> Items
        {
            get => items;
            set => SetProperty(ref items, value);
        }

        public double PanelWidth
        {
            get => panelWidth;
            set => SetProperty(ref panelWidth, value);
        }

        public string Select { get; private set; }

        public RelayCommand<string> SelectItem { get; }
        #endregion

        private void SelectItemMethod(string obj)
        {
            Select = obj;
            Close?.Invoke();
        }
    }
}
