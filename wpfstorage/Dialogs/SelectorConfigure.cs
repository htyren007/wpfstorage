﻿using System;
using System.Collections.Generic;

namespace WPFStorage.Dialogs
{
    public class SelectorConfigure : IConfigureBase
    {
        private SelectorViewModel model;

        public string SelectItem => model.Select;

        public SelectorConfigure()
        {
            model = new SelectorViewModel();
        }

        public SelectorConfigure Question(string value)
        {
            model.Question = value;
            return this;
        }

        public SelectorConfigure Title(string value)
        {
            model.Title = value;
            return this;
        }

        public SelectorConfigure AddItem(string item)
        {
            model.Items.Add(item);
            return this;
        }
        public SelectorConfigure AddRange(IEnumerable<string> items)
        {
            model.Items.AddRange(items);
            return this;
        }

        public void Show()
        {
            ShowSelectorBoxModel(model);
            //return model.Select;
        }

        public void ShowAsunc()
        {
            throw new NotImplementedException();
        }

        private static void ShowSelectorBoxModel(SelectorViewModel model)
        {
            SelectorWindow window = new SelectorWindow(model);
            bool? res = window.ShowDialog();
            if (res == true)
            {
                model.PressOk = true;
            }
        }
    }
}