﻿using System;

namespace WPFStorage.Dialogs
{
    public class LargeTextConfigure
    {
        private LargeTextModel model;

        internal LargeTextConfigure()
        {
            model = new LargeTextModel();
        }

        public void Show()
        {
            LagreText window = CreateWindow();
            window.ShowDialog();
        }

        private LagreText CreateWindow()
        {
            LagreText window = new LagreText();
            window.DataContext = model;
            window.Width = model.Width;
            window.Height = model.Height;
            model.CloseWindow = () => window.Close();
            return window;
        }

        public void Open()
        {
            LagreText window = CreateWindow();
            window.Show();
        }

        /// <summary>
        /// Задать название окна по умолчанию "<c>Окно ввода</c>"
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public LargeTextConfigure SetTitle(string title)
        {
            model.Title = title;
            return this;
        }

        /// <summary>
        /// Задать текст вопроса или сообщение, по умолчанию "<c>Введите значение</c>"
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public LargeTextConfigure SetText(string text)
        {
            model.Text = text;
            return this;
        }

        /// <summary>
        /// Задать текст кнопки Ok, по умолчанию "<c>Ввод</c>"
        /// </summary>
        /// <param name="okText"></param>
        /// <returns></returns>
        public LargeTextConfigure SetOkText(string okText)
        {
            model.OkText = okText;
            return this;
        }

        public LargeTextConfigure SetCancelText(string text)
        {
            model.CancelText = text;
            return this;
        }

        public LargeTextConfigure SetSize(double width, double height)
        {
            model.Width = width;
            model.Height = height;
            return this;
        }
    }
}