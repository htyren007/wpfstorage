﻿namespace WPFStorage.Dialogs
{
    public class LargeTextModel : ADialogModelBase
    {
        private double height = 250;
        private double width = 400;
        private string text;

        public LargeTextModel()
        {

        }

        public double Height { get => height; set => SetProperty(ref height, value); }
        public double Width { get => width; set => SetProperty(ref width, value); }
        public string Text { get => text; set => SetProperty(ref text, value); }

        protected override void Cancel()
        {
        }

        protected override void Ok()
        {
        }
    }
}
