﻿using System;

namespace WPFStorage.Dialogs
{
    public class DigitBoxConfigure : ConfigureBase<DigitBoxModel, DigitBoxConfigure>
    {
        public DigitBoxConfigure()
        {
            model = new DigitBoxModel();
        }

        public bool ShowDialog(out double value)
        {
            WinBox.ShowNumericBoxModel(model);
            value = model.Value;
            return model.PressOk;
        }

        public DigitBoxConfigure SetIsInteger(bool isInteger)
        {
            model.IsInteger = isInteger;
            return this;
        }

        public DigitBoxConfigure SetStep(double step)
        {
            model.Inkermenter = step;
            return this;
        }

        public DigitBoxConfigure SetStartValue(double digit)
        {
            model.Value = digit;
            return this;
        }


        public DigitBoxConfigure SetMinimum(double digit)
        {
            model.Minimum = digit;
            return this;
        }

        public DigitBoxConfigure SetMaximum(double digit)
        {
            model.Maximum = digit;
            return this;
        }

        public override void Show()
        {
            throw new NotImplementedException();
        }

        public override void ShowAsunc()
        {
            throw new NotImplementedException();
        }
    }
}