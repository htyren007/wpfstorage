﻿using System;
using System.ComponentModel;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    public class DigitBoxModel : BoxModelBase
    {
        private double digit = 0.01d;
        private double minimum = 0;
        private double maximum = 100;
        private double inkermenter = 0.0001;
        private bool isInteger = false;
        /// <summary>Точность в символах полсе запятой</summary>
        private int accuracy = 4;

        public double Value
        {
            get => digit;
            set
            {
                if (value < Minimum)
                    SetProperty(ref this.digit, Minimum);
                else if (value > Maximum)
                    SetProperty(ref this.digit, Maximum);
                else
                    SetProperty(ref this.digit, Math.Round(value, accuracy));
            }
        }

        public double Inkermenter
        {
            get => inkermenter;
            set
            {
                SetProperty(ref inkermenter, value);
                accuracy = GetDecimalDigitsCount(value);
            }
        }

        public bool IsInteger
        {
            get => isInteger;
            set => SetProperty(ref isInteger, value);
        }

        public double Minimum
        {
            get => minimum;
            set => SetProperty(ref this.minimum, value);
        }

        public double Maximum
        {
            get => maximum;
            set => SetProperty(ref this.maximum, value);
        }

        static int GetDecimalDigitsCount(double number)
        {
            string[] str = number.ToString(new System.Globalization.NumberFormatInfo() { NumberDecimalSeparator = "." }).Split('.');
            return str.Length == 2 ? str[1].Length : 0;
        }
    }
}