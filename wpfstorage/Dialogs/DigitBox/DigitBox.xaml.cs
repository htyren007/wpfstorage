﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace WPFStorage.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class DigitBox : Window
    {
        private DigitBoxModel model;
        private DispatcherTimer timer;

        public DigitBox(DigitBoxModel model)
        {
            InitializeComponent();
            this.model = model;

            model.Success = () =>
            {
                DialogResult = true;
                Close();
            };
            model.Close = () =>
            {
                DialogResult = false;
                Close();
            };
            DataContext = model;
            Loaded += (a, b) =>
            {
                model.Initialize();
            };

            if (timer == null)
            {
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            }
        }

        //private void inputPreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        //{
        //    e.Handled = !((Char.IsDigit(e.Text, 0) 
        //        || ((e.Text == CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString()) 
        //        && (DecimalSeparatorCount(((TextBox)sender).Text) < 1))));
        //}

        //public int DecimalSeparatorCount(string s)
        //{
        //    string substr = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0].ToString();
        //    int count = (s.Length - s.Replace(substr, "").Length) / substr.Length;
        //    return count;
        //}

        //private void Up_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    timer.Stop();
        //    timer.Tick -= TickUp;
        //    model?.UpNumeric.Execute(null);
        //}

        //private void Up_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    timer.Tick += TickUp;
        //    timer.Start();

        //}

        //private void Down_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    timer.Stop();
        //    timer.Tick -= TickDown;
        //    model?.DownNumeric.Execute(null);
        //}

        //private void Down_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    timer.Tick += TickDown;
        //    timer.Start();
        //}

        //private void TickUp(object sender, EventArgs e) => model?.UpNumeric.Execute(null);

        //private void TickDown(object sender, EventArgs e) => model?.DownNumeric.Execute(null);

        //private void input_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        //{
        //    model?.Amount(e.Delta>0?1:-1 * model.Inkermenter);
        //}
    }
}
