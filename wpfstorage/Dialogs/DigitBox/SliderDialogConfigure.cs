﻿using System;

namespace WPFStorage.Dialogs
{
    public class SliderDialogConfigure
    {
        private SliderDialogModel model;

        public SliderDialogConfigure() => model = new SliderDialogModel();

        /// <summary>
        /// Показать сконфигурированный диалог
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Show(out double value)
        {
            SliderWin window = new SliderWin(model);
            var res = window.ShowDialog();
            value = model.Value;
            return model.PressOk;
        }

        /// <summary>
        /// Задать название окна по умолчанию "<c>Окно ввода</c>"
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetTitle(string title)
        {
            model.Title = title;
            return this;
        }

        /// <summary>
        /// Задать текст вопроса или сообщение, по умолчанию "<c>Введите значение</c>"
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetQuestion(string question)
        {
            model.Question = question;
            return this;
        }

        /// <summary>
        /// Задать текст кнопки Ok, по умолчанию "<c>Ввод</c>"
        /// </summary>
        /// <param name="okText"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetOkText(string okText)
        {
            model.OkText = okText;
            return this;
        }

        /// <summary>
        /// Задать текст кнопки Cancel, по умолчанию "<c>Отмена</c>"
        /// </summary>
        /// <param name="cancelText"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetCancelText(string cancelText)
        {
            model.CancelText = cancelText;
            return this;
        }

        /// <summary>
        /// Задать Видимость кнопки Ok, по умолчанию "<c>true</c>"
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetActiveOk(bool isActive)
        {
            model.OkIsActive = isActive;
            return this;
        }
        /// <summary>
        /// Задать Видимость кнопки Cancel, по умолчанию "<c>true</c>"
        /// </summary>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetActiveCancel(bool isActive)
        {
            model.CancelIsActive = isActive;
            return this;
        }
        /// <summary>
        /// Устнановить шаг с которым будет менятся значение
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetStep(double step)
        {
            model.Step = step;
            return this;
        }
        /// <summary>
        /// Установить визуальный шаг штрихов на шкале элемента
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        public SliderDialogConfigure SetTickStep(double step)
        {
            model.TickStep = step;
            return this;
        }

        public SliderDialogConfigure SetMinimum(double minimum)
        {
            model.Minimum = minimum;
            return this;
        }

        public SliderDialogConfigure SetMaximum(double maximum)
        {
            model.Maximum = maximum;
            return this;
        }
    }
}