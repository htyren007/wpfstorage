﻿using System;
using System.Windows;
using WPFStorage.Base;

namespace WPFStorage.Dialogs
{
    public class SliderDialogModel : ADialogModelBase
    {
        private string question = "Введите значение";
        private double digit;
        private double maximum = 100;
        private double minimum = 0;
        //private double countStep;
        private double step = 0.001;
        private double tickStep = 1;

        public SliderDialogModel(): base()
        {
            Title = "Окно ввода";
            OkText = "Ввод";
        }

        /// <summary>
        /// Вопрос на форме
        /// </summary>
        public string Question { get => question; set => SetProperty(ref question, value); }

        /// <summary>
        /// Значение ввода пользователем и начальное значение диалога
        /// </summary>
        public double Value
        {
            get => digit;
            set
            {
                double delta = value % Step;
                if (delta < Step / 2)
                    SetProperty(ref digit, value - delta);
                else
                    SetProperty(ref digit, value - delta + Step);

            }
        }
        public double Minimum { get => minimum; set => SetProperty(ref minimum, value); }
        public double Maximum { get => maximum; set => SetProperty(ref maximum, value); }
        public double Step { get => step; set => SetProperty(ref step, value); }
        public double TickStep { get => tickStep; set => SetProperty(ref tickStep, value); }

        protected override void Ok()
        {
        }

        protected override void Cancel()
        {
        }
    }
}